package com.fast.orm.generator;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.fast.orm.generator.bean.FileCreateConfig;

public class FastFileUtil {
    private FileCreateConfig config = new FileCreateConfig();

    /**
     * 需要生成的表名称
     *
     * @param tables 多个表用逗号隔开,如果需要生成数据库中所有的表,参数为all
     */
    public void setCreateTables(String... tables) {
        config.setCreateTables(tables);
    }

    /**
     * 需要生成的模板文件类型,使用FileCreateConfig.CodeCreateModule枚举,多个用逗号隔开
     *
     * @param modules 模板文件类型
     */
    public void setNeedModules(FileCreateConfig.CodeCreateModule... modules) {
        config.setNeedModules(modules);
    }

    /**
     * 如果是多模块项目,需要使用此项
     *
     * @param childModuleName 指定在哪个模块下创建模板文件
     */
    public void setChildModuleName(String childModuleName) {
        config.setChildModuleName(childModuleName);
    }

    /**
     * 是否使用了Lombok插件
     *
     * @param useLombok 不进行设置的话默认false
     */
    public void setUseLombok(Boolean useLombok) {
        config.setUseLombok(useLombok);
    }


    /**
     * 是否使用内联pojo
     *
     * @param exInlinePojo 内联pojo交货
     */
    public void setExInlinePojo(Boolean exInlinePojo) {
        config.setExInlinePojo(exInlinePojo);
    }

    /**
     * 是否在DTO上生成Swagger2注解
     *
     * @param useDTOSwagger2 不进行设置的话默认false
     */
    public void setUseDTOSwagger2(Boolean useDTOSwagger2) {
        config.setUseDTOSwagger2(useDTOSwagger2);
    }

    /**
     * 是否在POJO上生成Swagger2注解
     *
     * @param usePOJOSwagger2 不进行设置的话默认false
     */
    public void setUsePOJOSwagger2(Boolean usePOJOSwagger2) {
        config.setUsePOJOSwagger2(usePOJOSwagger2);
    }

    /**
     * DTO是否继承POJO
     *
     * @param dtoExtendsPOJO 不进行设置的话默认false
     */
    public void setDtoExtendsPOJO(Boolean dtoExtendsPOJO) {
        config.setDtoExtendsPOJO(dtoExtendsPOJO);
    }


    /**
     * 生成模板的包路径
     *
     * @param basePackage 包路径地址 xxx.xxx.xxx
     */
    public void setBasePackage(String basePackage) {
        config.setBasePackage(basePackage);
    }

    /**
     * 设置数据库连接信息
     *
     * @param url         数据库连接
     * @param user        用户名
     * @param password    密码
     * @param driverClass 数据库驱动
     */
    public void setDBInfo(String url, String user, String password, String driverClass) {
        config.setDBInfo(url,user,password,driverClass);
    }

    /**
     * 是否过滤表前缀信息
     *
     * @param prefix        生成文件时候是否过滤表前缀信息，ord_orders = orders
     * @param prefixFileDir 是否通过前缀信息生成不同的文件目录,ord_orders 会为将orders生成的模板存储在ord目录下
     * @param prefixName    过滤指定前缀,如果不指定传 null
     */
    public void setPrefix(Boolean prefix, Boolean prefixFileDir, String prefixName) {
        config.setPrefix(prefix, prefixFileDir,prefixName);
    }

   public void create(){
       if (StrUtil.isBlank(config.getUrl())
       ||StrUtil.isBlank(config.getUser())
       ||StrUtil.isBlank(config.getPassword())
       ||StrUtil.isBlank(config.getDriverClass())) {
           throw new RuntimeException("数据库信息未设置,请调用setDBInfo方法进行设置");
       }
       if (StrUtil.isBlank(config.getBasePackage())) {
           throw new RuntimeException("包路径未设置,请调用setBasePackage方法进行设置");
       }
       if (CollUtil.isEmpty(config.getNeedModules())) {
           config.setNeedModules(FileCreateConfig.CodeCreateModule.Base);
       }
       if (CollUtil.isEmpty(config.getCreateTables())) {
           config.setCreateTables("all");
       }
       TableFileCreateUtils.create(config);
   }


}
