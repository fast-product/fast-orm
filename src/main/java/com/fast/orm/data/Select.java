package com.fast.orm.data;

public class Select<T> extends BaseMapper<T>{
    private final T fields;
    public Select(DataPackage dataPackage, T fields) {
        this.dataPackage = dataPackage;
        this.fields = fields;
    }

    /**
     * 查询特定字段
     *
     * @return 条件操作工具
     */
    public T show() {
        ConditionSetting.setSelectField(dataPackage, DataPackage.SelectField.SelectType.FIELD);
        return fields;
    }

    /**
     * 屏蔽特定字段
     *
     * @return 条件操作工具
     */
    public T hide() {
        ConditionSetting.hideField(dataPackage);
        return fields;
    }

    /**
     * 字段求和
     *
     * @return 条件操作工具
     */
    public T sum() {
        ConditionSetting.setSelectField(dataPackage, DataPackage.SelectField.SelectType.SUM);
        return fields;
    }

    /**
     * 字段求平均值
     *
     * @return 条件操作工具
     */
    public T avg() {
        ConditionSetting.setSelectField(dataPackage, DataPackage.SelectField.SelectType.AVG);
        return fields;
    }

    /**
     * 字段求平均值
     *
     * @return 条件操作工具
     */
    public T min() {
        ConditionSetting.setSelectField(dataPackage, DataPackage.SelectField.SelectType.MIN);
        return fields;
    }

    /**
     * 字段求平均值
     *
     * @return 条件操作工具
     */
    public T max() {
        ConditionSetting.setSelectField(dataPackage, DataPackage.SelectField.SelectType.MAX);
        return fields;
    }

    /**
     * 字段去重
     *
     * @return 条件操作工具
     */
    public T distinct() {
        ConditionSetting.setSelectField(dataPackage, DataPackage.SelectField.SelectType.DISTINCT);
        return fields;
    }

    /**
     * 分组
     *
     * @return 条件操作工具
     */
    public T groupBy() {
        ConditionSetting.setGroupField(dataPackage);
        return fields;
    }

}
