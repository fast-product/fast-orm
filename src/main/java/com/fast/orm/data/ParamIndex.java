package com.fast.orm.data;

public class ParamIndex {
    private long index = 0L;

    public long addAndGet() {
        return ++this.index;
    }
}
