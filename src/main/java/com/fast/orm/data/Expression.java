package com.fast.orm.data;

import cn.hutool.core.util.EnumUtil;

import java.util.HashMap;
import java.util.Map;

public enum Expression {
    /**
     * 条件表达式
     */
    Equal("equal", " = "),
    NotEqual("notEqual", " != "),
    Like("like", " LIKE "),
    NotLike("notLike", " NOT LIKE "),
    Match(" MATCH", " AGAINST"),
    NotMatch(" NOT MATCH", " AGAINST"),
    In("in", " IN "),
    NotIn("notIn", " NOT IN "),
    Between("between", " BETWEEN "),
    NotBetween("notBetween", " NOT BETWEEN "),
    Null("null", " IS NULL "),
    NotNull("notNull", " IS NOT NULL "),
    Greater("greater", " > "),
    GreaterOrEqual("greaterOrEqual", " >= "),
    Less("less", " < "),
    LessOrEqual("lessOrEqual", " <= "),
    LeftBracket("leftBracket", "("),
    RightBracket("rightBracket", ")"),
    SQL("sql", "自定义SQL添加"),
    Obj("object", " = "),
    FindInSet("findInSet", "FIND_IN_SET"),
    NotFindInSet("findInSet", " NOT FIND_IN_SET");

    public final String name;
    public final String expression;

    Expression(String name, String expression) {
        this.name = name;
        this.expression = expression;
    }

    private static final Map<String, Expression> EX_MAP = new HashMap<>();

    public static Expression getExpression(String name) {
        Expression ex = EX_MAP.get(name);
        if (ex != null) {
            return ex;
        }
        ex = EnumUtil.likeValueOf(Expression.class, name);
        EX_MAP.put(name, ex);
        return ex;
    }
}
