package com.fast.orm.data;

public class BaseMapper<T> {
    protected DataPackage dataPackage;

    public DataPackage getDataPackage() {
        return dataPackage;
    }
}
