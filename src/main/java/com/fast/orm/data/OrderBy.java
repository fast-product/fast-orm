package com.fast.orm.data;

public class OrderBy<T> {
    private final DataPackage dataPackage;
    private final T fields;

    public OrderBy(DataPackage dataPackage, T fields) {
        this.dataPackage = dataPackage;
        this.fields = fields;
    }

    public T asc() {
        dataPackage.getOrderByList().add(new DataPackage.OrderByQuery(dataPackage.getField(), DataPackage.OrderByQuery.OrderByType.ASC));
        return fields;
    }
    public T desc() {
        dataPackage.getOrderByList().add(new DataPackage.OrderByQuery(dataPackage.getField(), DataPackage.OrderByQuery.OrderByType.DESC));
        return fields;
    }
}
