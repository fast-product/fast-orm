package com.fast.orm.data;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.fast.orm.jdbc.MySqlUtil;
import com.fast.orm.many.JoinDirection;
import org.apache.commons.lang3.ArrayUtils;

import java.util.List;

public class ConditionSetting {
    public static void setSelectField(DataPackage dataPackage, DataPackage.SelectField.SelectType selectType) {
        dataPackage.getSelectFieldList().add(new DataPackage.SelectField(dataPackage.getField(), selectType));
    }

    public static void setGroupField(DataPackage dataPackage) {
        dataPackage.getGroupByQueryList().add(new DataPackage.GroupByQuery(dataPackage.getField()));
    }


    public static <T>T joinCondition(T t, DataPackage.Way way, DataPackage.JoinInfo joinInfo, BaseMapper baseMapper) {
        joinInfo.getAnd().append(way.expression).append(MySqlUtil.whereCondition(baseMapper.getDataPackage().getWhereList().get(0), baseMapper.getDataPackage()));
        baseMapper.getDataPackage().getWhereList().remove(baseMapper.getDataPackage().getWhereList().size()-1);
        return t;
    }

    public static <T>T joinOn(T t,DataPackage.JoinInfo joinInfo,BaseMapper leftCondition, BaseMapper rightCondition) {
        joinInfo.setLeftField(leftCondition.getDataPackage().getField());
        joinInfo.setRightField(rightCondition.getDataPackage().getField());
        joinInfo.setRightDataPackage(rightCondition.getDataPackage());
        return t;
    }

    public static <T>T joinOn(T t,DataPackage.JoinInfo joinInfo,DataPackage right,String leftField, String rightField) {
        joinInfo.setLeftField(leftField);
        joinInfo.setRightField(rightField);
        joinInfo.setRightDataPackage(right);
        return t;
    }

    public static <T> DataPackage.JoinInfo joinInfo(DataPackage dataPackage, JoinDirection direction, BaseMapper<T> join, String[] tableAlias) {
        dataPackage.showTableName();
        join.getDataPackage().showTableName();
        DataPackage.JoinInfo joinInfo = new DataPackage.JoinInfo();
        joinInfo.setLeftDataPackage(join.getDataPackage());
        if (ArrayUtils.isNotEmpty(tableAlias)) {
            join.getDataPackage().setTableAlias(tableAlias[0]);
        }
        joinInfo.setDirection(direction);
        dataPackage.getJoinList().add(joinInfo);
        return joinInfo;
    }

    public static void setBracket(DataPackage dataPackage, Expression expression, DataPackage.Way way) {
        DataPackage.ConditionData conditionData = new DataPackage.ConditionData();
        conditionData.setWay(way);
        conditionData.setExpression(expression);
        dataPackage.addWhere(conditionData);
    }

    public static void setObj(DataPackage dataPackage, Object obj) {
        DataPackage.ConditionData conditionData = new DataPackage.ConditionData();
        conditionData.setValue(obj);
        conditionData.setWay(DataPackage.Way.AND);
        conditionData.setExpression(Expression.Obj);
        dataPackage.addWhere(conditionData);
    }
    public static void hideField(DataPackage dataPackage) {
        List<DataPackage.SelectField> selectFieldList = dataPackage.getSelectFieldList();
        String field = dataPackage.getField();
        if (CollUtil.isEmpty(selectFieldList)) {
            for (String fieldName : dataPackage.getTableMapper().getFieldNames()) {
                if (!StrUtil.equals(field, fieldName)) {
                    selectFieldList.add(new DataPackage.SelectField(fieldName, DataPackage.SelectField.SelectType.FIELD));
                }
            }
            return;
        }
        Integer removeIndex = null;
        for (int i = 0; i < selectFieldList.size(); i++) {
            DataPackage.SelectField selectField = selectFieldList.get(i);
            if (StrUtil.equals(selectFieldList.get(i).getField(), field) &&
                    ObjectUtil.equals(selectField.getType(), DataPackage.SelectField.SelectType.FIELD)) {
                removeIndex = i;
            }
        }
        if (removeIndex != null) {
            selectFieldList.remove(removeIndex.intValue());
        }
    }


}
