package com.fast.orm.data;

import java.util.Map;

public class Update<T> {
    private final DataPackage dataPackage;
    private final T fields;

    public Update(DataPackage dataPackage, T fields) {
        this.dataPackage = dataPackage;
        this.fields = fields;
    }

    /**
     * 更新
     * tableColumnName = val
     * 如:equal(10) 则 tableColumnName = 10
     *
     * @param val  参数
     * @return 条件封装
     */
    public T set(Object val) {
        dataPackage.getUpdateFieldMap().put(dataPackage.getField(),
                new DataPackage.UpdateField(dataPackage.getField(),val,
                        DataPackage.UpdateField.UpdateType.EQUAL));
        return fields;
    }
    /**
     * this加value加法运算
     * tableColumnName = tableColumnName + val
     * 如:thisAdd(10) 则 tableColumnName = tableColumnName + 10
     *
     * @param val  参数
     * @return 条件封装
     */
    public T thisAdd(Object val) {
        dataPackage.getUpdateFieldMap().put(dataPackage.getField(),
                new DataPackage.UpdateField(dataPackage.getField(),val,
                        DataPackage.UpdateField.UpdateType.THIS_ADD));
        return fields;
    }

    /**
     * this加value减法运算
     * tableColumnName = tableColumnName - val
     * 如:thisMinus(10) 则 tableColumnName = tableColumnName - 10
     *
     * @param val  参数
     * @return 条件封装
     */
    public T thisSbu(Number val) {
        dataPackage.getUpdateFieldMap().put(dataPackage.getField(),
                new DataPackage.UpdateField(dataPackage.getField(),val,
                        DataPackage.UpdateField.UpdateType.THIS_SBU));
        return fields;
    }

    /**
     * this加value乘法运算
     * tableColumnName = tableColumnName * val
     * 如:thisMinus(10) 则 tableColumnName = tableColumnName * 10
     *
     * @param val  参数
     * @return 条件封装
     */
    public T thisMul(Number val) {
        dataPackage.getUpdateFieldMap().put(dataPackage.getField(),
                new DataPackage.UpdateField(dataPackage.getField(),val,
                        DataPackage.UpdateField.UpdateType.THIS_MUL));
        return fields;
    }

    /**
     * this加value除法运算
     * tableColumnName = tableColumnName / val
     * 如:thisMinus(10) 则 tableColumnName = tableColumnName / 10
     *
     * @param val  参数
     * @return 条件封装
     */
    public T thisDiv(Number val) {
        dataPackage.getUpdateFieldMap().put(dataPackage.getField(),
                new DataPackage.UpdateField(dataPackage.getField(),val,
                        DataPackage.UpdateField.UpdateType.THIS_DIV));
        return fields;
    }

    /**
     * this加value取模运算
     * tableColumnName = tableColumnName % val
     * 如:thisMinus(10) 则 tableColumnName = tableColumnName % 10
     *
     * @param val  参数
     * @return 条件封装
     */
    public T thisModulo(Number val) {
        dataPackage.getUpdateFieldMap().put(dataPackage.getField(),
                new DataPackage.UpdateField(dataPackage.getField(),val,
                        DataPackage.UpdateField.UpdateType.THIS_MODULO));
        return fields;
    }
    /**
     * 自定义字段运算
     * tableColumnName = customizeVal
     * 如:customize("user_age + 1") 则 tableColumnName = user_age + 1
     * 可以使用${参数名}  new HashMap(参数名,数据) 进行占位
     *
     * @param customizeVal 操作列后的自定义信息
     * @param data         条件
     * @return 条件封装
     */
    public T customize(String customizeVal, Map<String, Object> data) {
        dataPackage.getUpdateFieldMap().put(dataPackage.getField(),
                new DataPackage.UpdateField(dataPackage.getField(),customizeVal,
                        DataPackage.UpdateField.UpdateType.THIS_MODULO,data));
        return fields;
    }

}
