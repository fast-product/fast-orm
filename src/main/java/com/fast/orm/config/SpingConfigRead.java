package com.fast.orm.config;

import cn.hutool.core.util.StrUtil;
import com.fast.orm.logs.SqlLogLevel;
import com.fast.orm.utils.SpringUtil;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class SpingConfigRead {

    static {
        read();
    }

    private static void read() {
        Environment env = SpringUtil.getBean(Environment.class);
        if (env == null) {
            return;
        }
        String createTimeField = env.getProperty("fast.orm.create-time");
        if (StrUtil.isNotBlank(createTimeField)) {
            FastConfig.openAutoSetCreateTime(createTimeField);
        }
        String updateTimeField = env.getProperty("fast.orm.update-time");
        if (StrUtil.isNotBlank(updateTimeField)) {
            FastConfig.openAutoSetUpdateTime(updateTimeField);
        }
        String logicDeleteField = env.getProperty("fast.orm.logic-delete");
        if (StrUtil.isNotBlank(logicDeleteField)) {
            FastConfig.openLogicDelete(logicDeleteField, Boolean.TRUE);
        }
        String cache = env.getProperty("fast.orm.cache");
        if (StrUtil.isNotBlank(cache) && StrUtil.equals(cache, "true")) {
            FastConfig.openCache(10L, TimeUnit.SECONDS);
        }
        String log = env.getProperty("fast.orm.log");
        if (StrUtil.isNotBlank(log) && StrUtil.equals(log, "true")) {
            FastConfig.openSqlPrint(SqlLogLevel.INFO, true, true);
        }
    }

}
