package com.fast.orm.aspect;

import com.fast.orm.data.DataPackage;

import java.util.List;

public interface FastExpander {

    /**
     * Dao执行前的操作
     *
     * @param dataPackage 目标对象
     */
    void before(DataPackage dataPackage);

    /**
     * Dao执行后的操作
     *
     * @param dataPackage 目标对象
     */
    void after(DataPackage dataPackage);

    /**
     * 执行场景
     * @return INSERT,SELECT,UPDATE,DELETE
     */
    List<ExpanderOccasion> occasion();


}
