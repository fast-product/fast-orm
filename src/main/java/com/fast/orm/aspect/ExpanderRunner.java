package com.fast.orm.aspect;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Singleton;
import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import com.fast.orm.data.DataPackage;

import java.util.ArrayList;
import java.util.List;

public class ExpanderRunner {

    private static final List<Class<FastExpander>> INSERT_OCCASION = new ArrayList<>();
    private static final List<Class<FastExpander>> DELETE_OCCASION = new ArrayList<>();
    private static final List<Class<FastExpander>> UPDATE_OCCASION = new ArrayList<>();
    private static final List<Class<FastExpander>> SELECT_OCCASION = new ArrayList<>();
    private static Boolean isExpander = Boolean.FALSE;


    public static void addExpander(Class<FastExpander> expanderClass) {
        FastExpander fastExpander = Singleton.get(expanderClass);
        if (fastExpander == null || CollUtil.isEmpty(fastExpander.occasion())) {
            return;
        }
        for (ExpanderOccasion occasion : fastExpander.occasion()) {
            if (occasion.method.equals(ExpanderOccasion.INSERT.method)) {
                INSERT_OCCASION.add(expanderClass);
                continue;
            }
            if (occasion.method.equals(ExpanderOccasion.DELETE.method)) {
                DELETE_OCCASION.add(expanderClass);
                continue;
            }
            if (occasion.method.equals(ExpanderOccasion.UPDATE.method)) {
                UPDATE_OCCASION.add(expanderClass);
                continue;
            }
            if (occasion.method.equals(ExpanderOccasion.SELECT.method) || occasion.method.equals(ExpanderOccasion.COUNT.method)) {
                SELECT_OCCASION.add(expanderClass);
            }
        }
        isExpander = Boolean.TRUE;
    }

    public static void runBeforeExpander(DataPackage dataPackage, String methodName) {
        if (!isExpander) {
            return;
        }
        List<Class<FastExpander>> expanders = getExpanders(dataPackage, methodName);
        if (CollUtil.isEmpty(expanders)) {
            return;
        }

        for (Class<FastExpander> expanderClass : expanders) {
            FastExpander fastExpander = Singleton.get(expanderClass);
            fastExpander.before(dataPackage);
        }
    }

    public static void runAfterExpander(DataPackage dataPackage, String methodName) {
        if (!isExpander) {
            return;
        }
        List<Class<FastExpander>> expanders = getExpanders(dataPackage, methodName);
        if (CollUtil.isEmpty(expanders)) {
            return;
        }

        for (Class<FastExpander> expanderClass : expanders) {
            Singleton.get(expanderClass).after(dataPackage);
        }
    }


    private static List<Class<FastExpander>> getExpanders(DataPackage dataPackage, String methodName) {
        List<Class<FastExpander>> expanders = null;
        if (StrUtil.equals(methodName, ExpanderOccasion.INSERT.method)) {
            expanders = INSERT_OCCASION;
        } else if (methodName.equals(ExpanderOccasion.DELETE.method)) {
            expanders = DELETE_OCCASION;
        } else if (methodName.equals(ExpanderOccasion.UPDATE.method)) {
            if (BooleanUtil.isTrue(dataPackage.getLogicDelete())) {
                expanders = DELETE_OCCASION;
            } else {
                expanders = UPDATE_OCCASION;
            }
        } else if (methodName.equals(ExpanderOccasion.SELECT.method) || methodName.equals(ExpanderOccasion.COUNT.method)) {
            expanders = SELECT_OCCASION;
        }
        return expanders;
    }


}
