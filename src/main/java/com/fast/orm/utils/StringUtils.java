package com.fast.orm.utils;

import cn.hutool.core.util.StrUtil;

public class StringUtils {
    public static boolean eqSuffix(String str, String suffix) {
        if (str == null || str.length() < 1) {
            return false;
        }
        return StrUtil.equals(StrUtil.sub(str, str.length() - 1, str.length()), suffix);
    }
}
