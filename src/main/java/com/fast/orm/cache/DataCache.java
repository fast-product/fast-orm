package com.fast.orm.cache;

import cn.hutool.core.text.StrBuilder;
import cn.hutool.crypto.digest.MD5;
import com.alibaba.fastjson.JSONObject;
import com.fast.orm.config.FastAttributes;
import com.fast.orm.data.DataPackage;
import com.fast.orm.mapper.TableMapper;

import java.sql.SQLOutput;
import java.util.List;

/**
 * 缓存实现,目前支持三种缓存
 * 在需要进行缓存的Bean中加入注解
 * 1:@FastRedisCache Redis缓存
 * 2:@FastStaticCache 本地缓存
 * 默认参数为框架设置的缓存时间和类型
 * 缓存可选参数
 * 1:(Long 秒) 如@FastRedisCache(60L) 缓存60秒
 * 2:(cacheTime = 时间,cacheTimeType = TimeUnit) 如@FastRedisCache(cacheTime =1L,cacheTimeType = TimeUnit.HOURS) 缓存1小时
 * <p>
 * 缓存在进行新增,更新,删除是会自动刷新
 *
 * @param <T> 缓存操作对象泛型
 * @author 张亚伟 https://github.com/kaixinzyw
 */
public class DataCache<T> {

    public final static String PREFIX_NAME = "fast_cache_";
    private final static String SEPARATOR = ":";
    public final static String FIND_COUNT = "findCount";
    public final static String FIND_ALL = "findAll";
    public final static String update = "findAll";
    /**
     * 缓存键名
     */
    private StrBuilder keyName;
    /**
     * 表映射关系
     */
    private DataPackage dataPackage;


    public static <T> DataCache<T> init(DataPackage dataPackage) {
        DataCache<T> dataCache = new DataCache<>();
        dataCache.dataPackage = dataPackage;
        dataCache.keyName = StrBuilder.create(PREFIX_NAME, dataCache.dataPackage.getTableMapper().getTableName(), SEPARATOR, getKey(dataPackage));
        return dataCache;
    }

    private static String getKey(DataPackage dataPackage) {
        return MD5.create().digestHex(dataPackage.getSql());
    }


    /**
     * 从缓存中回去列表数据
     *
     * @return 查询结果
     */
    public List<T> get() {
        if (dataPackage.getTableMapper().getCacheType().equals(DataCacheType.StaticCache)) {
            return StaticCacheImpl.get(dataPackage.getTableMapper().getTableName(),
                    dataPackage.getTableMapper().getCacheTime(), dataPackage.getTableMapper().getCacheTimeType(),
                    keyName.toString());
        } else if (dataPackage.getTableMapper().getCacheType().equals(DataCacheType.RedisCache)) {
            return RedisCacheImpl.get(keyName.toString());
        }
        return null;
    }

    /**
     * 设置缓存
     *
     * @param ts 设置的参数
     */
    public void set(List<T> ts) {
        if (dataPackage.getTableMapper().getCacheType().equals(DataCacheType.StaticCache)) {
            StaticCacheImpl.set(ts, dataPackage.getTableMapper().getTableName(),
                    dataPackage.getTableMapper().getCacheTime(), dataPackage.getTableMapper().getCacheTimeType(),
                    keyName.toString());
        } else if (dataPackage.getTableMapper().getCacheType().equals(DataCacheType.RedisCache)) {
            RedisCacheImpl.set(ts, dataPackage.getTableMapper().getCacheTime(), dataPackage.getTableMapper().getCacheTimeType(), keyName.toString());
        }
    }


    /**
     * 更新缓存,对象的缓存信息
     *
     * @param dataPackage 数据操作对象映射信息
     */
    public static void upCache(DataPackage dataPackage) {
        if (!dataPackage.getTableMapper().getOpenCache()) {
            return;
        }
        if (dataPackage.getTableMapper().getCacheType().equals(DataCacheType.StaticCache)) {
            StaticCacheImpl.update(dataPackage.getTableMapper());
        } else if (dataPackage.getTableMapper().getCacheType().equals(DataCacheType.RedisCache)) {
            RedisCacheImpl.update(dataPackage.getTableMapper());
        }
    }


}
