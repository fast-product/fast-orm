package com.fast.orm.cache;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSONObject;
import com.fast.orm.config.FastAttributes;
import com.fast.orm.mapper.TableMapper;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class RedisCacheImpl {

    private final static String WILDCARD = ":*";

    private static StringRedisTemplate redisTemplate() {
        return FastAttributes.getStringRedisTemplate();
    }

    public static List get(String keyName) {
        StringRedisTemplate redisTemplate = redisTemplate();
        String ostr = redisTemplate.opsForValue().get(keyName);
        if (ostr == null) {
            return null;
        }
        return JSONObject.parseArray(ostr);
    }

    public static <T> void set(List<T> ts, Long cacheTime, TimeUnit cacheTimeType, String keyName) {
        StringRedisTemplate redisTemplate = redisTemplate();
        redisTemplate.opsForValue().set(keyName, JSONObject.toJSONString(ts), cacheTime, cacheTimeType);
    }

    public static void update(TableMapper tableMapper) {
        StringRedisTemplate redisTemplate = redisTemplate();
        Set keys = redisTemplate.keys(DataCache.PREFIX_NAME + tableMapper.getTableName() + WILDCARD);
        if (CollUtil.isNotEmpty(keys)) {
            redisTemplate.delete(keys);
        }
    }


}
