package com.fast.orm.cache;

import com.fast.orm.mapper.TableMapper;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class StaticCacheImpl {

    /**
     * 本地缓存的单条列表数据
     * 适用于@FastStaticCache
     */
    private static final Map<String, Cache<String, List<?>>> cacheMap = new HashMap<>();

    private static  Cache<String, List<?>> createCache(Long cacheTime, TimeUnit cacheTimeType) {
        return CacheBuilder.newBuilder()
                .expireAfterWrite(cacheTime, cacheTimeType)
                .recordStats()
                .build();
    }

    public static Cache<String, List<?>> create(String tableName, Long cacheTime, TimeUnit cacheTimeType) {
        Cache<String, List<?>> cache = cacheMap.get(tableName);
        if (cache == null) {
            synchronized ((tableName).intern()) {
                if (cacheMap.get(tableName) != null) {
                    return cacheMap.get(tableName);
                }
                cacheMap.put(tableName, createCache(cacheTime,cacheTimeType));
            }
            cache = cacheMap.get(tableName);
        }
        return cache;
    }

    public static <T> List<T> get(String tableName, Long cacheTime, TimeUnit cacheTimeType, String keyName) {
        Cache<String, List<?>> cache = create(tableName,cacheTime,cacheTimeType);
        if (cache.size() == 0) {
            return null;
        }
        return (List<T>) cache.getIfPresent(keyName);

    }

    public static <T> void set(List<T> ts, String tableName, Long cacheTime, TimeUnit cacheTimeType, String keyName) {
        Cache<String, List<?>> list = create(tableName,cacheTime,cacheTimeType);
        list.put(keyName, ts);
    }

    public static void update(TableMapper tableMapper) {
        cacheMap.remove(tableMapper.getTableName());
    }
}
