package com.fast.orm.exec;

import com.fast.orm.data.DataPackage;
import com.fast.orm.utils.SqlTemplateUtil;

import java.util.Map;

/**
 * 自定义SQL执行器
 *
 * @param <T> 自定义SQL操作的对象泛型
 * @author 张亚伟 https://github.com/kaixinzyw
 */
public class FastCustomQuery<T> {


    private FastCustomQuery() {
    }

    /**
     * 自定义SQL执行器初始化,如需使用,必须调用此方法进行初始化创建
     *
     * @param clazz  自定义SQL操作的类
     * @param sql    自定义sql语句,如果有占位符,使用#{参数名}进行描述 例:select * from xxx where userName=${userName}
     * @param params 占位符参数,如果使用占位符进行条件参数封装,必须传入条件参数 如上,需要使用Map put("userName","XXX")
     * @param <T>    自定义SQL操作的对象泛型
     * @return 自定义SQL执行器
     */
    public static <T> SelectExecution<T> create(Class<T> clazz, String sql, Map<String,Object> params) {
        DataPackage dataPackage = new DataPackage(clazz);
        dataPackage.setCustomSql(sql);
        dataPackage.getParamMap().putAll(params);
        return new SelectExecution<>(dataPackage);
    }

    /**
     * 自定义SQL执行器初始化,如需使用,必须调用此方法进行初始化创建
     *
     * @param clazz  自定义SQL操作的类
     * @param path   resource 目录下的文件,文件编码必须为UTF-8
     * @param params 占位符参数,如果使用占位符进行条件参数封装,必须传入条件参数 如上,需要使用Map put("userName","XXX")
     * @param <T>    自定义SQL操作的对象泛型
     * @return 自定义SQL执行器
     */
    public static <T> SelectExecution<T> createResource(Class<T> clazz, String path, Map<String,Object> params) {
        return create(clazz, SqlTemplateUtil.getSql(path, params), params);
    }

}
