package com.fast.orm.exec;

import cn.hutool.core.util.StrUtil;
import com.fast.orm.data.DataPackage;
import com.fast.orm.jdbc.MySqlUtil;

/**
 * 查询方法Sql语句拼接
 *
 * @author 张亚伟 https://github.com/kaixinzyw
 */
public class SelectProvider {

    public static void findAll(DataPackage dataPackage) {
        if (StrUtil.isNotEmpty(dataPackage.getCustomSql())) {
            dataPackage.setSql(dataPackage.getCustomSql());
            if (dataPackage.getLimit().length() > 0) {
                dataPackage.setSql(dataPackage.getSql()+ MySqlUtil.CRLF+dataPackage.getLimit());
            }
            return;
        }
        dataPackage.setSql(MySqlUtil.getQuery(dataPackage));
    }

    public static void findCount(DataPackage dataPackage) {
        if (StrUtil.isNotEmpty(dataPackage.getCustomSql())) {
            if (!StrUtil.containsIgnoreCase(dataPackage.getCustomSql(), MySqlUtil.COUNT)) {
                dataPackage.setSql(MySqlUtil.countQueryInfoReplace(dataPackage.getCustomSql()));
            } else {
                dataPackage.setSql(dataPackage.getCustomSql());
            }
            return;
        }
        dataPackage.setSql(MySqlUtil.getCount(dataPackage));
    }


}
