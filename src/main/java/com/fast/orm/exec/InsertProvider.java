package com.fast.orm.exec;

import com.fast.orm.data.DataPackage;
import com.fast.orm.jdbc.MySqlUtil;
import com.fast.orm.utils.FastValueUtil;

/**
 * 扩展插入方法Sql语句拼接
 *
 * @author 张亚伟 https://github.com/kaixinzyw
 */
public class InsertProvider {

    public static void insert(DataPackage dataPackage) {
        FastValueUtil.setInsertData(dataPackage);
        dataPackage.setSql(MySqlUtil.insertSql(dataPackage));
    }
}
