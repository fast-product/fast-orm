package com.fast.orm.exec;

import com.fast.orm.data.DataPackage;
import com.fast.orm.jdbc.MySqlUtil;

/**
 * 删除方法Sql语句拼接
 *
 * @author 张亚伟 https://github.com/kaixinzyw
 */
public class DeleteProvider {

    public static void delete(DataPackage dataPackage) {
        dataPackage.setSql(MySqlUtil.getDeleteSql(dataPackage));
    }
}
