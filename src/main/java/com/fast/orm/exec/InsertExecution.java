package com.fast.orm.exec;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.fast.orm.cache.DataCache;
import com.fast.orm.data.Expression;
import com.fast.orm.jdbc.JdbcImpl;
import com.fast.orm.data.DataPackage;
import com.fast.orm.utils.FastValueUtil;

import java.util.Collection;
import java.util.List;

public class InsertExecution<POJO> {

    private final DataPackage dataPackage;

    public InsertExecution(DataPackage dataPackage) {
        this.dataPackage = dataPackage;
    }

    /**
     * 新增数据
     *
     * @param pojo 需要新增的数据,会对框架设置的主键字段进行赋值
     * @return 是否新增成功
     */
    public POJO insert(POJO pojo) {
        insert(CollUtil.newArrayList(pojo));
        return pojo;
    }

    /**
     * 批量新增数据
     *
     * @param pojos 需要新增的数据,会对框架设置的主键字段进行赋值
     * @return 是否新增成功
     */
    public List<POJO> insert(Collection<POJO> pojos) {
        dataPackage.getInsertList().addAll(pojos);
        List insert = JdbcImpl.insert(dataPackage);
        return insert;
    }

    /**
     * 新增或更新操作
     *
     * @param pojo 如果主键有值则进行更新操作,主键为空则进行新增操作,参数为空不进行更新
     * @return 新增结果
     */
    public POJO insertOrUpdateByPrimaryKey(POJO pojo) {
        String primaryKeyField = dataPackage.getTableMapper().getPrimaryKeyField();
        if (StrUtil.isBlank(primaryKeyField)) {
            throw new RuntimeException(dataPackage.getTableMapper().getTableName() + ": 未设置主键!!!");
        }
        Object fieldValue = BeanUtil.getFieldValue(pojo, primaryKeyField);
        if (fieldValue == null) {
            return insert(pojo);
        } else {
            FastValueUtil.setUpdateBeanValue(dataPackage,pojo);
            dataPackage.addWhere(new DataPackage.ConditionData(dataPackage.getTableMapper().getPrimaryKeyField(),fieldValue,DataPackage.Way.AND, Expression.Equal));
            new UpdateExecution(dataPackage).update();
            return pojo;
        }
    }

    /**
     * 新增或更新操作
     *
     * @param pojo 如果主键有值则进行更新操作,主键为空则进行新增操作,参数为空则也进行更新
     * @return 新增结果
     */
    public POJO insertOrUpdateOverallByPrimaryKey(POJO pojo) {
        String primaryKeyField = dataPackage.getTableMapper().getPrimaryKeyField();
        if (StrUtil.isBlank(primaryKeyField)) {
            throw new RuntimeException(dataPackage.getTableMapper().getTableName() + ": 未设置主键!!!");
        }
        Object fieldValue = BeanUtil.getFieldValue(pojo, primaryKeyField);
        if (fieldValue == null) {
            return insert(pojo);
        } else {
            FastValueUtil.setUpdateBeanValue(dataPackage,pojo);
            dataPackage.addWhere(new DataPackage.ConditionData(dataPackage.getTableMapper().getPrimaryKeyField(),fieldValue,DataPackage.Way.AND, Expression.Equal));
            new UpdateExecution(dataPackage).update();
            return pojo;
        }
    }
}
