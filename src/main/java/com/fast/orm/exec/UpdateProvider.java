package com.fast.orm.exec;

import com.fast.orm.data.DataPackage;
import com.fast.orm.jdbc.MySqlUtil;

/**
 * 更新方法Sql语句拼接
 *
 * @author 张亚伟 https://github.com/kaixinzyw
 */
public class UpdateProvider {

    public static void update(DataPackage dataPackage) {
        dataPackage.setSql(MySqlUtil.getUpdate(dataPackage));
    }
}
