package com.fast.orm.mapper;

public enum PrimaryKeyType {
    /**
     * 主键类型 32位UUID和自增
     */
    OBJECTID,AUTO
}
