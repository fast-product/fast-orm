package com.fast.orm.mapper;

import cn.hutool.core.collection.CollUtil;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Fast类加载器
 *
 * @author 张亚伟
 */
@Component
public class FastBeanListener implements ApplicationContextAware {

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        Map<String, Object> map = applicationContext.getBeansWithAnnotation(FastBean.class);
        if (CollUtil.isNotEmpty(map)) {
            for (Object value : map.values()) {
                try {
                    TableMapperUtil.getTableMappers(value.getClass());
                }catch (Exception ignored){}
            }
        }
    }
}