package com.fast.orm.mapper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 表别名
 *
 * @author 张亚伟
 */
@Target({ElementType.TYPE})
@Retention(RUNTIME)
public @interface TableAlias {
    String value() default "";
}
