package com.fast.orm.jdbc;


import cn.hutool.core.util.ClassUtil;
import com.alibaba.fastjson.JSONObject;
import com.fast.orm.data.DataPackage;
import com.fast.orm.utils.BeanCopyUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

/**
 * SpringJDBC 对象映射
 *
 * @author 张亚伟 https://github.com/kaixinzyw
 */
public class JdbcRowMapper implements RowMapper<JSONObject> {
    /**
     * {@inheritDoc}.
     */
    @Override
    public JSONObject mapRow(ResultSet rs, int arg1) {
        try {
            final ResultSetMetaData metaData = rs.getMetaData();
            int columnLength = metaData.getColumnCount();
            JSONObject jsonObject = new JSONObject();
            for (int i = 1; i <= columnLength; i++) {
                jsonObject.put(metaData.getColumnLabel(i), rs.getObject(i));
            }
            return jsonObject;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}