package com.fast.orm.many;

import com.fast.orm.config.Null;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 多对多
 *
 * @author zyw
 */
@Target(FIELD)
@Retention(RUNTIME)
public @interface JoinQuery {
    /**
     * 连接表别名
     *
     * @return {@link String}
     */
    String tableAlias() default "";

    /**
     * 连接列的名字
     *
     * @return {@link String}
     */
    String columnName() default "";

    /**
     * 连接表别名
     *
     * @return {@link String}
     */
    Class value() default Null.class;


}
