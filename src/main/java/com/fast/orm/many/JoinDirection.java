package com.fast.orm.many;

public enum JoinDirection {
    /**
     * 连接方向
     */
    LEFT_JOIN("LEFT JOIN "), RIGHT_JOIN("RIGHT JOIN "), INNER_JOIN("INNER JOIN ");
    public final String direction;

    JoinDirection(String direction) {
        this.direction = direction;
    }
}
