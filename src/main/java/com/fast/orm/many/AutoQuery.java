package com.fast.orm.many;

import com.fast.orm.config.Null;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author kaixi
 */
@Target(FIELD)
@Retention(RUNTIME)
public @interface AutoQuery {
    String thisField() default "";
    String targetField() default "";
    String queryField() default "";
    Class value() default Null.class;
}
