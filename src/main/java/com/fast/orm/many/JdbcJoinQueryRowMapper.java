package com.fast.orm.many;


import com.alibaba.fastjson.JSONObject;
import com.fast.orm.data.DataPackage;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * SpringJDBC 对象映射
 *
 * @author 张亚伟 https://github.com/kaixinzyw
 */
public class JdbcJoinQueryRowMapper implements RowMapper<JSONObject> {

    private DataPackage dataPackage;
    Map<Object,//主表主键
            Map<String,//所在字段
                    Map<Object,//集合数据ID
                            JSONObject//集合数据
                            >>> collectionData;

    public JdbcJoinQueryRowMapper(DataPackage dataPackage,Map<Object, Map<String, Map<Object, JSONObject>>> collectionData) {
        this.dataPackage = dataPackage;
        this.collectionData = collectionData;
    }



    /**
     * {@inheritDoc}.
     */
    @Override
    public JSONObject mapRow(ResultSet rs, int arg1) throws SQLException {
            final ResultSetMetaData metaData = rs.getMetaData();
            int columnLength = metaData.getColumnCount();
            Map<String, JSONObject> resultMap = new HashMap<>();
            for (int i = 1; i <= columnLength; i++) {
                String tableName = metaData.getTableName(i);
                JSONObject json = resultMap.get(tableName);
                if (json == null) {
                    json = new JSONObject();
                    resultMap.put(tableName, json);
                }
                json.put(metaData.getColumnLabel(i), rs.getObject(i));
            }
            JSONObject mainObj = resultMap.get(dataPackage.getTableAlias());

            //字段值映射
            Map<String, Map<String, String>> tableAliasFieldMap = dataPackage.getReturnMapper().getTableAliasFieldMap();
            for (String tableName : tableAliasFieldMap.keySet()) {
                JSONObject mapperObj = resultMap.get(tableName);
                Map<String, String> fieldColumnMapperMap = tableAliasFieldMap.get(tableName);
                for (String mapperColumnName : fieldColumnMapperMap.keySet()) {
                    Object data = mapperObj.get(fieldColumnMapperMap.get(mapperColumnName));
                    if (data != null) {
                        mainObj.put(fieldColumnMapperMap.get(mapperColumnName), data);
                    }
                }
            }
            //对象值映射
            if (dataPackage.getFastJoinQueryInfoList() != null) {
                for (FastJoinQueryInfo fastJoinQueryInfo : dataPackage.getFastJoinQueryInfoList()) {
                    if (!fastJoinQueryInfo.getCollectionType()) {
                        JSONObject join = resultMap.get(fastJoinQueryInfo.getJoinTableAlias());
                        if (join.get(fastJoinQueryInfo.getJoinColumnName())!=null) {
                            JSONObject target = resultMap.get(fastJoinQueryInfo.getThisTableAlias());
                            target.put(fastJoinQueryInfo.getFieldName(), join);
                        }
                    } else {
                        if (fastJoinQueryInfo.getThisTableAlias().equals(dataPackage.getTableAlias())) {
                            JSONObject jsonObject = resultMap.get(fastJoinQueryInfo.getJoinTableAlias());
                            if (jsonObject.get(fastJoinQueryInfo.getJoinColumnName())==null) {
                                continue;
                            }
                            Object mainValue = mainObj.get(dataPackage.getTableMapper().getPrimaryKeyTableField());
                            Map<String, Map<Object, JSONObject>> fieldData = collectionData.get(mainValue);
                            if (fieldData == null) {
                                fieldData = new HashMap<>();
                                collectionData.put(mainValue,fieldData);
                            }
                            Map<Object, JSONObject> dataMap = fieldData.get(fastJoinQueryInfo.getFieldName());
                            if (dataMap == null) {
                                dataMap=new HashMap<>();
                                fieldData.put(fastJoinQueryInfo.getFieldName(),dataMap);
                            }
                            dataMap.put(jsonObject.get(fastJoinQueryInfo.getJoinPrimaryKeyColumnName()),jsonObject);
                        }
                    }
                }
            }

            return mainObj;
        }



//    /**
//     * {@inheritDoc}.
//     */
//    @Override
//    public Map<String, JSONObject> mapRow(ResultSet rs, int arg1) {
//        try {
//            final ResultSetMetaData metaData = rs.getMetaData();
//            int columnLength = metaData.getColumnCount();
//            Map<String, JSONObject> resultMap = new HashMap<>();
//            for (int i = 1; i <= columnLength; i++) {
//                String tableName = metaData.getTableName(i);
//                JSONObject json = resultMap.get(tableName);
//                if (json == null) {
//                    json = new JSONObject();
//                    resultMap.put(tableName, json);
//                }
//                json.put(metaData.getColumnLabel(i), rs.getObject(i));
//            }
//            JSONObject mainObj = resultMap.get(dataPackage.getTableAlias());
//
//            //字段值映射
//            Map<String, Map<String, String>> tableAliasFieldMap = dataPackage.getReturnMapper().getTableAliasFieldMap();
//            for (String tableName : tableAliasFieldMap.keySet()) {
//                JSONObject mapperObj = resultMap.get(tableName);
//                Map<String, String> fieldColumnMapperMap = tableAliasFieldMap.get(tableName);
//                for (String mapperColumnName : fieldColumnMapperMap.keySet()) {
//                    mainObj.put(fieldColumnMapperMap.get(mapperColumnName), mapperObj.get(mapperColumnName));
//                }
//            }
//
//            //对象值映射
//            if (dataPackage.getFastJoinQueryInfoList() != null) {
//                for (FastJoinQueryInfo fastJoinQueryInfo : dataPackage.getFastJoinQueryInfoList()) {
//                    if (!fastJoinQueryInfo.getCollectionType()) {
//                        JSONObject target = resultMap.get(fastJoinQueryInfo.getThisTableAlias());
//                        JSONObject join = resultMap.get(fastJoinQueryInfo.getJoinTableAlias());
//                        target.put(fastJoinQueryInfo.getFieldName(),join);
//                    }
//                }
//            }
//
//
//            return resultMap;
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }
}