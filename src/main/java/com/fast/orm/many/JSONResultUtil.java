package com.fast.orm.many;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.fast.orm.data.DataPackage;
import com.fast.orm.mapper.TableMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 张亚伟
 */
public class JSONResultUtil {

    public static List<JSONObject> change(DataPackage dataPackage, List<Map<String, JSONObject>> jdbcMapResult) {
        if (CollUtil.isEmpty(jdbcMapResult)) {
            return null;
        }
        Map<String, Map<String, String>> tableAliasFieldMap = dataPackage.getReturnMapper().getTableAliasFieldMap();
        Map<String, List<JSONObject>> dataMap = new HashMap<>();
        for (Map<String, JSONObject> objectMap : jdbcMapResult) {
            for (String tableName : objectMap.keySet()) {
                List<JSONObject> list = dataMap.get(tableName);
                if (list == null) {
                    list = new ArrayList<>();
                    dataMap.put(tableName, list);
                }
                JSONObject object = objectMap.get(tableName);
                list.add(object);
                if (tableName.equals(dataPackage.getTableAlias())) {
                    if (CollUtil.isNotEmpty(tableAliasFieldMap)) {
                        for (String tableAlias : tableAliasFieldMap.keySet()) {
                            Map<String, String> mapper = tableAliasFieldMap.get(tableAlias);
                            if (CollUtil.isNotEmpty(mapper)) {
                                JSONObject jsonObject = objectMap.get(tableAlias);
                                if (jsonObject!=null) {
                                    for (String columnName : mapper.keySet()) {
                                        String name = mapper.get(columnName);
                                        object.put(name,jsonObject.get(name));
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
        dataMap.put(dataPackage.getTableAlias(), CollUtil.distinct(dataMap.get(dataPackage.getTableAlias())));
        for (FastJoinQueryInfo fastJoinQueryInfo : dataPackage.getFastJoinQueryInfoList()) {
            List<JSONObject> thisTableObjectList = dataMap.get(fastJoinQueryInfo.getThisTableAlias());
            if (CollUtil.isEmpty(thisTableObjectList)) {
                continue;
            }
            for (JSONObject joinTableObject : thisTableObjectList) {
                Object thisId = joinTableObject.get(fastJoinQueryInfo.getThisColumnName());
                List<JSONObject> joinDataList = dataMap.get(fastJoinQueryInfo.getJoinTableAlias());
                if (CollUtil.isEmpty(joinDataList)) {
                    continue;
                }
                List<JSONObject> joinGroupList = new ArrayList<>();
                for (JSONObject joinData : joinDataList) {
                    if (ObjectUtil.equal(joinData.get(fastJoinQueryInfo.getJoinColumnName()), thisId)) {
                        joinGroupList.add(joinData);
                    }
                }
                joinGroupList = CollUtil.distinct(joinGroupList);
                if (CollUtil.isNotEmpty(joinGroupList)) {
                    if (fastJoinQueryInfo.getCollectionType()) {
                        joinTableObject.put(fastJoinQueryInfo.getFieldName(), joinGroupList);
                    } else {
                        for (JSONObject joinObj : joinGroupList) {
                            if (ObjectUtil.equal(joinObj.get(fastJoinQueryInfo.getJoinColumnName()), thisId)) {
                                joinTableObject.put(fastJoinQueryInfo.getFieldName(), joinObj);
                                break;
                            }
                        }
                    }

                }
            }
        }
        return dataMap.get(dataPackage.getTableAlias());
    }
}
