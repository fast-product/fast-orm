package com.fast.orm.many;

import com.fast.orm.mapper.TableMapper;

import java.lang.reflect.Field;
import java.util.List;

public class AutoQueryInfo {
    private TableMapper queryTableMapper;
    private String fieldName;
    private String thisColumnName;
    private String thisFieldName;
    private String targetColumnName;
    private String targetFieldName;
    private String valueFieldName;
    private String valueColumnName;
    private Boolean isCollectionType;
    private Boolean isBasicType;

    private List<AutoQueryInfo> autoQueryInfoList;

    public TableMapper getQueryTableMapper() {
        return queryTableMapper;
    }

    public void setQueryTableMapper(TableMapper queryTableMapper) {
        this.queryTableMapper = queryTableMapper;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }


    public String getThisColumnName() {
        return thisColumnName;
    }

    public void setThisColumnName(String thisColumnName) {
        this.thisColumnName = thisColumnName;
    }

    public String getTargetColumnName() {
        return targetColumnName;
    }

    public void setTargetColumnName(String targetColumnName) {
        this.targetColumnName = targetColumnName;
    }

    public Boolean getCollectionType() {
        return isCollectionType;
    }

    public void setCollectionType(Boolean collectionType) {
        isCollectionType = collectionType;
    }

    public String getThisFieldName() {
        return thisFieldName;
    }

    public void setThisFieldName(String thisFieldName) {
        this.thisFieldName = thisFieldName;
    }

    public String getTargetFieldName() {
        return targetFieldName;
    }

    public void setTargetFieldName(String targetFieldName) {
        this.targetFieldName = targetFieldName;
    }

    public Boolean getBasicType() {
        return isBasicType;
    }

    public void setBasicType(Boolean basicType) {
        isBasicType = basicType;
    }

    public String getValueFieldName() {
        return valueFieldName;
    }

    public void setValueFieldName(String valueFieldName) {
        this.valueFieldName = valueFieldName;
    }

    public String getValueColumnName() {
        return valueColumnName;
    }

    public void setValueColumnName(String valueColumnName) {
        this.valueColumnName = valueColumnName;
    }

    public List<AutoQueryInfo> getAutoQueryInfoList() {
        return autoQueryInfoList;
    }

    public void setAutoQueryInfoList(List<AutoQueryInfo> autoQueryInfoList) {
        this.autoQueryInfoList = autoQueryInfoList;
    }
}
