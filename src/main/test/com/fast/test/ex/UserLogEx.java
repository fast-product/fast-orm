package com.fast.test.ex;

import com.fast.orm.data.*;
import com.fast.orm.exec.DeleteExecution;
import com.fast.orm.exec.InsertExecution;
import com.fast.orm.exec.SelectExecution;
import com.fast.orm.exec.UpdateExecution;
import com.fast.orm.many.JoinDirection;
import com.fast.orm.utils.FastValueUtil;
import com.fast.test.pojo.UserLog;

/**
* 用户日志
*/
public class UserLogEx {
    public static UserLogInsert INSERT() {
        return new UserLogInsert();
    }
    public static UserLogDelete DELETE() {
        return new UserLogDelete();
    }
    public static UserLogUpdate UPDATE() {
        return new UserLogUpdate();
    }
    public static UserLogSelect<UserLog> SELECT() {
        return new UserLogSelect<>();
    }
    public static <R>UserLogSelect<R> SELECT(Class<R> returnClass) {
        return new UserLogSelect<>(returnClass);
    }
    public static class UserLogInsert{
        private final DataPackage dataPackage;
        public UserLogInsert() {
            this.dataPackage = new DataPackage(UserLog.class);
        }
        public InsertExecution<UserLog> EXEC() {
            return new InsertExecution<>(dataPackage);
        }
    }
    public static class UserLogDelete {
        private final DataPackage dataPackage;
        private UserLogDelete() {
            this.dataPackage = new DataPackage(UserLog.class);
        }
        public DeleteExecution EXEC() {
            return new DeleteExecution(dataPackage);
        }
        public UserLogWhere<DeleteExecution> WHERE() {
            return new UserLogWhere<>(dataPackage, new DeleteExecution(dataPackage));
        }
    }
    public static class UserLogUpdate extends UserLogFields<Update<UserLogUpdate>> {
        private UserLogUpdate() {
            this.dataPackage = new DataPackage(UserLog.class);
            this.t = new Update<>(dataPackage, this);
        }
        public UserLogUpdate set(UserLog pojo){
            FastValueUtil.setUpdateBeanValue(dataPackage, pojo);
            return this;
        }
        public UpdateExecution<UserLog> EXEC() {
            return new UpdateExecution<>(dataPackage);
        }
        public UserLogWhere<UpdateExecution<UserLog>> WHERE() {
            return new UserLogWhere<>(dataPackage, new UpdateExecution<>(dataPackage));
        }
    }
    public static class UserLogSelect<R> extends UserLogFields<Select<UserLogSelect<R>>> {
        private UserLogSelect() {
            this.dataPackage = new DataPackage(UserLog.class);
            this.t = new Select<>(dataPackage, this);
        }
        private UserLogSelect(Class<R> returnClass) {
            this.dataPackage = new DataPackage(UserLog.class,returnClass);
            this.t = new Select<>(dataPackage, this);
        }
        public <T>UserLogJoin<T,R> LEFT_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserLogJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.LEFT_JOIN,baseMapper, tableAlias));
        }
        public <T>UserLogJoin<T,R> RIGHT_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserLogJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.RIGHT_JOIN,baseMapper, tableAlias));
        }
        public <T>UserLogJoin<T,R> INNER_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserLogJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.INNER_JOIN,baseMapper, tableAlias));
        }
        public UserLogOrderBy<R> ORDER_BY() {
            return new UserLogOrderBy<>(dataPackage);
        }
        public SelectExecution<R> EXEC() {
            return new SelectExecution<>(dataPackage);
        }
        public UserLogWhere<SelectExecution<R>> WHERE() {
            return new UserLogWhere<>(dataPackage, new SelectExecution<>(dataPackage));
        }
    }

    public static class UserLogJoin<P,R> extends UserLogSelect<R>{
        private final DataPackage.JoinInfo joinInfo;
        public UserLogJoin(DataPackage dataPackage,DataPackage.JoinInfo joinInfo) {
            this.dataPackage = dataPackage;
            this.joinInfo = joinInfo;
        }
        public UserLogJoin<P,R> on(String leftField, String rightField) {
            return ConditionSetting.joinOn(this,joinInfo,dataPackage,leftField, rightField);
        }
        public UserLogJoin<P,R> on(BaseMapper<P> leftCondition, BaseMapper rightCondition) {
            return ConditionSetting.joinOn(this,joinInfo,leftCondition, rightCondition);
        }
        public UserLogJoin<P,R> and(BaseMapper<P> baseMapper){
            return ConditionSetting.joinCondition(this,DataPackage.Way.AND,joinInfo,baseMapper);
        }
        public UserLogJoin<P,R> or(BaseMapper<P> baseMapper){
             return ConditionSetting.joinCondition(this,DataPackage.Way.OR,joinInfo,baseMapper);
        }
    }

    public static class UserLogOrderBy<R> extends UserLogFields<OrderBy<UserLogOrderBy<R>>> {
        public UserLogOrderBy(DataPackage dataPackage) {
            this.dataPackage = dataPackage;
            this.t = new OrderBy<>(dataPackage, this);
        }
        public SelectExecution<R> EXEC() {
            return new SelectExecution<>(dataPackage);
        }
        public UserLogWhere<SelectExecution<R>> WHERE() {
            return new UserLogWhere<>(dataPackage, new SelectExecution<>(dataPackage));
        }
    }
    public static class UserLogWhere<F> extends UserLogFields<Where<UserLogWhere<F>, UserLog>> {
        private final F f;
        private UserLogWhere(DataPackage dataPackage, F f) {
            this.dataPackage = dataPackage;
            this.t = new Where<>(dataPackage, this);
            this.f = f;
        }
        public UserLogWhere<F> leftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.CUSTOM);
            return this;
        }
        public UserLogWhere<F> orLeftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.OR);
            return this;
        }
        public UserLogWhere<F> andLeftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.AND);
            return this;
        }
        public UserLogWhere<F> rightBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.RightBracket,DataPackage.Way.CUSTOM);
            return this;
        }
        public UserLogWhere<F> eqToObj(Object obj){
            ConditionSetting.setObj(dataPackage,obj);
            return this;
        }
        public UserLogWhere<F> closeLogicDeleteProtect(){
            dataPackage.setLogicDelete(Boolean.FALSE);
            return this;
        }
        public F EXEC() {
            return f;
        }
    }
    public static class UserLogFields<T> extends BaseMapper<UserLog>{
        protected T t;
        /**
        *主键
        */
        public T id(){
            dataPackage.setField("id");
            return t;
        }
        /**
        *用户ID
        */
        public T userId(){
            dataPackage.setField("userId");
            return t;
        }
        /**
        *日志内容
        */
        public T logInfo(){
            dataPackage.setField("logInfo");
            return t;
        }
        /**
        *创建时间
        */
        public T createTime(){
            dataPackage.setField("createTime");
            return t;
        }
        /**
        *更新时间
        */
        public T updateTime(){
            dataPackage.setField("updateTime");
            return t;
        }
        /**
        *是否删除
        */
        public T deleted(){
            dataPackage.setField("deleted");
            return t;
        }
    }
}