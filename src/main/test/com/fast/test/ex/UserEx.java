package com.fast.test.ex;

import com.fast.orm.data.*;
import com.fast.orm.exec.DeleteExecution;
import com.fast.orm.exec.InsertExecution;
import com.fast.orm.exec.SelectExecution;
import com.fast.orm.exec.UpdateExecution;
import com.fast.orm.many.JoinDirection;
import com.fast.orm.utils.FastValueUtil;
import com.fast.test.pojo.User;

/**
* 用户
*/
public class UserEx {
    public static UserInsert INSERT() {
        return new UserInsert();
    }
    public static UserDelete DELETE() {
        return new UserDelete();
    }
    public static UserUpdate UPDATE() {
        return new UserUpdate();
    }
    public static UserSelect<User> SELECT() {
        return new UserSelect<>();
    }
    public static <R>UserSelect<R> SELECT(Class<R> returnClass) {
        return new UserSelect<>(returnClass);
    }
    public static class UserInsert{
        private final DataPackage dataPackage;
        public UserInsert() {
            this.dataPackage = new DataPackage(User.class);
        }
        public InsertExecution<User> EXEC() {
            return new InsertExecution<>(dataPackage);
        }
    }
    public static class UserDelete {
        private final DataPackage dataPackage;
        private UserDelete() {
            this.dataPackage = new DataPackage(User.class);
        }
        public DeleteExecution EXEC() {
            return new DeleteExecution(dataPackage);
        }
        public UserWhere<DeleteExecution> WHERE() {
            return new UserWhere<>(dataPackage, new DeleteExecution(dataPackage));
        }
    }
    public static class UserUpdate extends UserFields<Update<UserUpdate>> {
        private UserUpdate() {
            this.dataPackage = new DataPackage(User.class);
            this.t = new Update<>(dataPackage, this);
        }
        public UserUpdate set(User pojo){
            FastValueUtil.setUpdateBeanValue(dataPackage, pojo);
            return this;
        }
        public UpdateExecution<User> EXEC() {
            return new UpdateExecution<>(dataPackage);
        }
        public UserWhere<UpdateExecution<User>> WHERE() {
            return new UserWhere<>(dataPackage, new UpdateExecution<>(dataPackage));
        }
    }
    public static class UserSelect<R> extends UserFields<Select<UserSelect<R>>> {
        private UserSelect() {
            this.dataPackage = new DataPackage(User.class);
            this.t = new Select<>(dataPackage, this);
        }
        private UserSelect(Class<R> returnClass) {
            this.dataPackage = new DataPackage(User.class,returnClass);
            this.t = new Select<>(dataPackage, this);
        }
        public <T>UserJoin<T,R> LEFT_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.LEFT_JOIN,baseMapper, tableAlias));
        }
        public <T>UserJoin<T,R> RIGHT_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.RIGHT_JOIN,baseMapper, tableAlias));
        }
        public <T>UserJoin<T,R> INNER_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.INNER_JOIN,baseMapper, tableAlias));
        }
        public UserOrderBy<R> ORDER_BY() {
            return new UserOrderBy<>(dataPackage);
        }
        public SelectExecution<R> EXEC() {
            return new SelectExecution<>(dataPackage);
        }
        public UserWhere<SelectExecution<R>> WHERE() {
            return new UserWhere<>(dataPackage, new SelectExecution<>(dataPackage));
        }
    }

    public static class UserJoin<P,R> extends UserSelect<R>{
        private final DataPackage.JoinInfo joinInfo;
        public UserJoin(DataPackage dataPackage,DataPackage.JoinInfo joinInfo) {
            this.dataPackage = dataPackage;
            this.joinInfo = joinInfo;
        }
        public UserJoin<P,R> on(String leftField, String rightField) {
            return ConditionSetting.joinOn(this,joinInfo,dataPackage,leftField, rightField);
        }
        public UserJoin<P,R> on(BaseMapper<P> leftCondition, BaseMapper rightCondition) {
            return ConditionSetting.joinOn(this,joinInfo,leftCondition, rightCondition);
        }
        public UserJoin<P,R> and(BaseMapper<P> baseMapper){
            return ConditionSetting.joinCondition(this,DataPackage.Way.AND,joinInfo,baseMapper);
        }
        public UserJoin<P,R> or(BaseMapper<P> baseMapper){
             return ConditionSetting.joinCondition(this,DataPackage.Way.OR,joinInfo,baseMapper);
        }
    }

    public static class UserOrderBy<R> extends UserFields<OrderBy<UserOrderBy<R>>> {
        public UserOrderBy(DataPackage dataPackage) {
            this.dataPackage = dataPackage;
            this.t = new OrderBy<>(dataPackage, this);
        }
        public SelectExecution<R> EXEC() {
            return new SelectExecution<>(dataPackage);
        }
        public UserWhere<SelectExecution<R>> WHERE() {
            return new UserWhere<>(dataPackage, new SelectExecution<>(dataPackage));
        }
    }
    public static class UserWhere<F> extends UserFields<Where<UserWhere<F>, User>> {
        private final F f;
        private UserWhere(DataPackage dataPackage, F f) {
            this.dataPackage = dataPackage;
            this.t = new Where<>(dataPackage, this);
            this.f = f;
        }
        public UserWhere<F> leftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.CUSTOM);
            return this;
        }
        public UserWhere<F> orLeftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.OR);
            return this;
        }
        public UserWhere<F> andLeftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.AND);
            return this;
        }
        public UserWhere<F> rightBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.RightBracket,DataPackage.Way.CUSTOM);
            return this;
        }
        public UserWhere<F> eqToObj(Object obj){
            ConditionSetting.setObj(dataPackage,obj);
            return this;
        }
        public UserWhere<F> closeLogicDeleteProtect(){
            dataPackage.setLogicDelete(Boolean.FALSE);
            return this;
        }
        public F EXEC() {
            return f;
        }
    }
    public static class UserFields<T> extends BaseMapper<User>{
        protected T t;
        /**
        *主键
        */
        public T id(){
            dataPackage.setField("id");
            return t;
        }
        /**
        *用户类型
        */
        public T userTypeId(){
            dataPackage.setField("userTypeId");
            return t;
        }
        /**
        *用户名
        */
        public T userName(){
            dataPackage.setField("userName");
            return t;
        }
        /**
        *年龄
        */
        public T age(){
            dataPackage.setField("age");
            return t;
        }
        /**
        *上级ID
        */
        public T pid(){
            dataPackage.setField("pid");
            return t;
        }
        /**
        *创建时间
        */
        public T createTime(){
            dataPackage.setField("createTime");
            return t;
        }
        /**
        *更新时间
        */
        public T updateTime(){
            dataPackage.setField("updateTime");
            return t;
        }
        /**
        *是否删除
        */
        public T deleted(){
            dataPackage.setField("deleted");
            return t;
        }
    }
}