package com.fast.test.ex;

import com.fast.orm.data.*;
import com.fast.orm.exec.DeleteExecution;
import com.fast.orm.exec.InsertExecution;
import com.fast.orm.exec.SelectExecution;
import com.fast.orm.exec.UpdateExecution;
import com.fast.orm.many.JoinDirection;
import com.fast.orm.utils.FastValueUtil;
import com.fast.test.pojo.UserTypeInfo;

/**
* 用户类型信息
*/
public class UserTypeInfoEx {
    public static UserTypeInfoInsert INSERT() {
        return new UserTypeInfoInsert();
    }
    public static UserTypeInfoDelete DELETE() {
        return new UserTypeInfoDelete();
    }
    public static UserTypeInfoUpdate UPDATE() {
        return new UserTypeInfoUpdate();
    }
    public static UserTypeInfoSelect<UserTypeInfo> SELECT() {
        return new UserTypeInfoSelect<>();
    }
    public static <R>UserTypeInfoSelect<R> SELECT(Class<R> returnClass) {
        return new UserTypeInfoSelect<>(returnClass);
    }
    public static class UserTypeInfoInsert{
        private final DataPackage dataPackage;
        public UserTypeInfoInsert() {
            this.dataPackage = new DataPackage(UserTypeInfo.class);
        }
        public InsertExecution<UserTypeInfo> EXEC() {
            return new InsertExecution<>(dataPackage);
        }
    }
    public static class UserTypeInfoDelete {
        private final DataPackage dataPackage;
        private UserTypeInfoDelete() {
            this.dataPackage = new DataPackage(UserTypeInfo.class);
        }
        public DeleteExecution EXEC() {
            return new DeleteExecution(dataPackage);
        }
        public UserTypeInfoWhere<DeleteExecution> WHERE() {
            return new UserTypeInfoWhere<>(dataPackage, new DeleteExecution(dataPackage));
        }
    }
    public static class UserTypeInfoUpdate extends UserTypeInfoFields<Update<UserTypeInfoUpdate>> {
        private UserTypeInfoUpdate() {
            this.dataPackage = new DataPackage(UserTypeInfo.class);
            this.t = new Update<>(dataPackage, this);
        }
        public UserTypeInfoUpdate set(UserTypeInfo pojo){
            FastValueUtil.setUpdateBeanValue(dataPackage, pojo);
            return this;
        }
        public UpdateExecution<UserTypeInfo> EXEC() {
            return new UpdateExecution<>(dataPackage);
        }
        public UserTypeInfoWhere<UpdateExecution<UserTypeInfo>> WHERE() {
            return new UserTypeInfoWhere<>(dataPackage, new UpdateExecution<>(dataPackage));
        }
    }
    public static class UserTypeInfoSelect<R> extends UserTypeInfoFields<Select<UserTypeInfoSelect<R>>> {
        private UserTypeInfoSelect() {
            this.dataPackage = new DataPackage(UserTypeInfo.class);
            this.t = new Select<>(dataPackage, this);
        }
        private UserTypeInfoSelect(Class<R> returnClass) {
            this.dataPackage = new DataPackage(UserTypeInfo.class,returnClass);
            this.t = new Select<>(dataPackage, this);
        }
        public <T>UserTypeInfoJoin<T,R> LEFT_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserTypeInfoJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.LEFT_JOIN,baseMapper, tableAlias));
        }
        public <T>UserTypeInfoJoin<T,R> RIGHT_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserTypeInfoJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.RIGHT_JOIN,baseMapper, tableAlias));
        }
        public <T>UserTypeInfoJoin<T,R> INNER_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserTypeInfoJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.INNER_JOIN,baseMapper, tableAlias));
        }
        public UserTypeInfoOrderBy<R> ORDER_BY() {
            return new UserTypeInfoOrderBy<>(dataPackage);
        }
        public SelectExecution<R> EXEC() {
            return new SelectExecution<>(dataPackage);
        }
        public UserTypeInfoWhere<SelectExecution<R>> WHERE() {
            return new UserTypeInfoWhere<>(dataPackage, new SelectExecution<>(dataPackage));
        }
    }

    public static class UserTypeInfoJoin<P,R> extends UserTypeInfoSelect<R>{
        private final DataPackage.JoinInfo joinInfo;
        public UserTypeInfoJoin(DataPackage dataPackage,DataPackage.JoinInfo joinInfo) {
            this.dataPackage = dataPackage;
            this.joinInfo = joinInfo;
        }
        public UserTypeInfoJoin<P,R> on(String leftField, String rightField) {
            return ConditionSetting.joinOn(this,joinInfo,dataPackage,leftField, rightField);
        }
        public UserTypeInfoJoin<P,R> on(BaseMapper<P> leftCondition, BaseMapper rightCondition) {
            return ConditionSetting.joinOn(this,joinInfo,leftCondition, rightCondition);
        }
        public UserTypeInfoJoin<P,R> and(BaseMapper<P> baseMapper){
            return ConditionSetting.joinCondition(this,DataPackage.Way.AND,joinInfo,baseMapper);
        }
        public UserTypeInfoJoin<P,R> or(BaseMapper<P> baseMapper){
             return ConditionSetting.joinCondition(this,DataPackage.Way.OR,joinInfo,baseMapper);
        }
    }

    public static class UserTypeInfoOrderBy<R> extends UserTypeInfoFields<OrderBy<UserTypeInfoOrderBy<R>>> {
        public UserTypeInfoOrderBy(DataPackage dataPackage) {
            this.dataPackage = dataPackage;
            this.t = new OrderBy<>(dataPackage, this);
        }
        public SelectExecution<R> EXEC() {
            return new SelectExecution<>(dataPackage);
        }
        public UserTypeInfoWhere<SelectExecution<R>> WHERE() {
            return new UserTypeInfoWhere<>(dataPackage, new SelectExecution<>(dataPackage));
        }
    }
    public static class UserTypeInfoWhere<F> extends UserTypeInfoFields<Where<UserTypeInfoWhere<F>, UserTypeInfo>> {
        private final F f;
        private UserTypeInfoWhere(DataPackage dataPackage, F f) {
            this.dataPackage = dataPackage;
            this.t = new Where<>(dataPackage, this);
            this.f = f;
        }
        public UserTypeInfoWhere<F> leftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.CUSTOM);
            return this;
        }
        public UserTypeInfoWhere<F> orLeftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.OR);
            return this;
        }
        public UserTypeInfoWhere<F> andLeftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.AND);
            return this;
        }
        public UserTypeInfoWhere<F> rightBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.RightBracket,DataPackage.Way.CUSTOM);
            return this;
        }
        public UserTypeInfoWhere<F> eqToObj(Object obj){
            ConditionSetting.setObj(dataPackage,obj);
            return this;
        }
        public UserTypeInfoWhere<F> closeLogicDeleteProtect(){
            dataPackage.setLogicDelete(Boolean.FALSE);
            return this;
        }
        public F EXEC() {
            return f;
        }
    }
    public static class UserTypeInfoFields<T> extends BaseMapper<UserTypeInfo>{
        protected T t;
        /**
        *主键
        */
        public T id(){
            dataPackage.setField("id");
            return t;
        }
        /**
        *用户类型ID
        */
        public T userTypeId(){
            dataPackage.setField("userTypeId");
            return t;
        }
        /**
        *类型信息
        */
        public T typeInfo(){
            dataPackage.setField("typeInfo");
            return t;
        }
        /**
        *创建时间
        */
        public T createTime(){
            dataPackage.setField("createTime");
            return t;
        }
        /**
        *更新时间
        */
        public T updateTime(){
            dataPackage.setField("updateTime");
            return t;
        }
        /**
        *是否删除
        */
        public T deleted(){
            dataPackage.setField("deleted");
            return t;
        }
    }
}