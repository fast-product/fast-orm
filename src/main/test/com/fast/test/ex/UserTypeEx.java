package com.fast.test.ex;

import com.fast.orm.data.*;
import com.fast.orm.exec.DeleteExecution;
import com.fast.orm.exec.InsertExecution;
import com.fast.orm.exec.SelectExecution;
import com.fast.orm.exec.UpdateExecution;
import com.fast.orm.many.JoinDirection;
import com.fast.orm.utils.FastValueUtil;
import com.fast.test.pojo.UserType;

/**
* 用户类型
*/
public class UserTypeEx {
    public static UserTypeInsert INSERT() {
        return new UserTypeInsert();
    }
    public static UserTypeDelete DELETE() {
        return new UserTypeDelete();
    }
    public static UserTypeUpdate UPDATE() {
        return new UserTypeUpdate();
    }
    public static UserTypeSelect<UserType> SELECT() {
        return new UserTypeSelect<>();
    }
    public static <R>UserTypeSelect<R> SELECT(Class<R> returnClass) {
        return new UserTypeSelect<>(returnClass);
    }
    public static class UserTypeInsert{
        private final DataPackage dataPackage;
        public UserTypeInsert() {
            this.dataPackage = new DataPackage(UserType.class);
        }
        public InsertExecution<UserType> EXEC() {
            return new InsertExecution<>(dataPackage);
        }
    }
    public static class UserTypeDelete {
        private final DataPackage dataPackage;
        private UserTypeDelete() {
            this.dataPackage = new DataPackage(UserType.class);
        }
        public DeleteExecution EXEC() {
            return new DeleteExecution(dataPackage);
        }
        public UserTypeWhere<DeleteExecution> WHERE() {
            return new UserTypeWhere<>(dataPackage, new DeleteExecution(dataPackage));
        }
    }
    public static class UserTypeUpdate extends UserTypeFields<Update<UserTypeUpdate>> {
        private UserTypeUpdate() {
            this.dataPackage = new DataPackage(UserType.class);
            this.t = new Update<>(dataPackage, this);
        }
        public UserTypeUpdate set(UserType pojo){
            FastValueUtil.setUpdateBeanValue(dataPackage, pojo);
            return this;
        }
        public UpdateExecution<UserType> EXEC() {
            return new UpdateExecution<>(dataPackage);
        }
        public UserTypeWhere<UpdateExecution<UserType>> WHERE() {
            return new UserTypeWhere<>(dataPackage, new UpdateExecution<>(dataPackage));
        }
    }
    public static class UserTypeSelect<R> extends UserTypeFields<Select<UserTypeSelect<R>>> {
        private UserTypeSelect() {
            this.dataPackage = new DataPackage(UserType.class);
            this.t = new Select<>(dataPackage, this);
        }
        private UserTypeSelect(Class<R> returnClass) {
            this.dataPackage = new DataPackage(UserType.class,returnClass);
            this.t = new Select<>(dataPackage, this);
        }
        public <T>UserTypeJoin<T,R> LEFT_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserTypeJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.LEFT_JOIN,baseMapper, tableAlias));
        }
        public <T>UserTypeJoin<T,R> RIGHT_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserTypeJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.RIGHT_JOIN,baseMapper, tableAlias));
        }
        public <T>UserTypeJoin<T,R> INNER_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserTypeJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.INNER_JOIN,baseMapper, tableAlias));
        }
        public UserTypeOrderBy<R> ORDER_BY() {
            return new UserTypeOrderBy<>(dataPackage);
        }
        public SelectExecution<R> EXEC() {
            return new SelectExecution<>(dataPackage);
        }
        public UserTypeWhere<SelectExecution<R>> WHERE() {
            return new UserTypeWhere<>(dataPackage, new SelectExecution<>(dataPackage));
        }
    }

    public static class UserTypeJoin<P,R> extends UserTypeSelect<R>{
        private final DataPackage.JoinInfo joinInfo;
        public UserTypeJoin(DataPackage dataPackage,DataPackage.JoinInfo joinInfo) {
            this.dataPackage = dataPackage;
            this.joinInfo = joinInfo;
        }
        public UserTypeJoin<P,R> on(String leftField, String rightField) {
            return ConditionSetting.joinOn(this,joinInfo,dataPackage,leftField, rightField);
        }
        public UserTypeJoin<P,R> on(BaseMapper<P> leftCondition, BaseMapper rightCondition) {
            return ConditionSetting.joinOn(this,joinInfo,leftCondition, rightCondition);
        }
        public UserTypeJoin<P,R> and(BaseMapper<P> baseMapper){
            return ConditionSetting.joinCondition(this,DataPackage.Way.AND,joinInfo,baseMapper);
        }
        public UserTypeJoin<P,R> or(BaseMapper<P> baseMapper){
             return ConditionSetting.joinCondition(this,DataPackage.Way.OR,joinInfo,baseMapper);
        }
    }

    public static class UserTypeOrderBy<R> extends UserTypeFields<OrderBy<UserTypeOrderBy<R>>> {
        public UserTypeOrderBy(DataPackage dataPackage) {
            this.dataPackage = dataPackage;
            this.t = new OrderBy<>(dataPackage, this);
        }
        public SelectExecution<R> EXEC() {
            return new SelectExecution<>(dataPackage);
        }
        public UserTypeWhere<SelectExecution<R>> WHERE() {
            return new UserTypeWhere<>(dataPackage, new SelectExecution<>(dataPackage));
        }
    }
    public static class UserTypeWhere<F> extends UserTypeFields<Where<UserTypeWhere<F>, UserType>> {
        private final F f;
        private UserTypeWhere(DataPackage dataPackage, F f) {
            this.dataPackage = dataPackage;
            this.t = new Where<>(dataPackage, this);
            this.f = f;
        }
        public UserTypeWhere<F> leftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.CUSTOM);
            return this;
        }
        public UserTypeWhere<F> orLeftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.OR);
            return this;
        }
        public UserTypeWhere<F> andLeftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.AND);
            return this;
        }
        public UserTypeWhere<F> rightBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.RightBracket,DataPackage.Way.CUSTOM);
            return this;
        }
        public UserTypeWhere<F> eqToObj(Object obj){
            ConditionSetting.setObj(dataPackage,obj);
            return this;
        }
        public UserTypeWhere<F> closeLogicDeleteProtect(){
            dataPackage.setLogicDelete(Boolean.FALSE);
            return this;
        }
        public F EXEC() {
            return f;
        }
    }
    public static class UserTypeFields<T> extends BaseMapper<UserType>{
        protected T t;
        /**
        *主键
        */
        public T id(){
            dataPackage.setField("id");
            return t;
        }
        /**
        *用户名
        */
        public T typeName(){
            dataPackage.setField("typeName");
            return t;
        }
        /**
        *创建时间
        */
        public T createTime(){
            dataPackage.setField("createTime");
            return t;
        }
        /**
        *更新时间
        */
        public T updateTime(){
            dataPackage.setField("updateTime");
            return t;
        }
        /**
        *是否删除
        */
        public T deleted(){
            dataPackage.setField("deleted");
            return t;
        }
    }
}