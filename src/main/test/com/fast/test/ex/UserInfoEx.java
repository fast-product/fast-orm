package com.fast.test.ex;

import com.fast.orm.data.*;
import com.fast.orm.exec.DeleteExecution;
import com.fast.orm.exec.InsertExecution;
import com.fast.orm.exec.SelectExecution;
import com.fast.orm.exec.UpdateExecution;
import com.fast.orm.many.JoinDirection;
import com.fast.orm.utils.FastValueUtil;
import com.fast.test.pojo.UserInfo;

/**
* 用户信息
*/
public class UserInfoEx {
    public static UserInfoInsert INSERT() {
        return new UserInfoInsert();
    }
    public static UserInfoDelete DELETE() {
        return new UserInfoDelete();
    }
    public static UserInfoUpdate UPDATE() {
        return new UserInfoUpdate();
    }
    public static UserInfoSelect<UserInfo> SELECT() {
        return new UserInfoSelect<>();
    }
    public static <R>UserInfoSelect<R> SELECT(Class<R> returnClass) {
        return new UserInfoSelect<>(returnClass);
    }
    public static class UserInfoInsert{
        private final DataPackage dataPackage;
        public UserInfoInsert() {
            this.dataPackage = new DataPackage(UserInfo.class);
        }
        public InsertExecution<UserInfo> EXEC() {
            return new InsertExecution<>(dataPackage);
        }
    }
    public static class UserInfoDelete {
        private final DataPackage dataPackage;
        private UserInfoDelete() {
            this.dataPackage = new DataPackage(UserInfo.class);
        }
        public DeleteExecution EXEC() {
            return new DeleteExecution(dataPackage);
        }
        public UserInfoWhere<DeleteExecution> WHERE() {
            return new UserInfoWhere<>(dataPackage, new DeleteExecution(dataPackage));
        }
    }
    public static class UserInfoUpdate extends UserInfoFields<Update<UserInfoUpdate>> {
        private UserInfoUpdate() {
            this.dataPackage = new DataPackage(UserInfo.class);
            this.t = new Update<>(dataPackage, this);
        }
        public UserInfoUpdate set(UserInfo pojo){
            FastValueUtil.setUpdateBeanValue(dataPackage, pojo);
            return this;
        }
        public UpdateExecution<UserInfo> EXEC() {
            return new UpdateExecution<>(dataPackage);
        }
        public UserInfoWhere<UpdateExecution<UserInfo>> WHERE() {
            return new UserInfoWhere<>(dataPackage, new UpdateExecution<>(dataPackage));
        }
    }
    public static class UserInfoSelect<R> extends UserInfoFields<Select<UserInfoSelect<R>>> {
        private UserInfoSelect() {
            this.dataPackage = new DataPackage(UserInfo.class);
            this.t = new Select<>(dataPackage, this);
        }
        private UserInfoSelect(Class<R> returnClass) {
            this.dataPackage = new DataPackage(UserInfo.class,returnClass);
            this.t = new Select<>(dataPackage, this);
        }
        public <T>UserInfoJoin<T,R> LEFT_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserInfoJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.LEFT_JOIN,baseMapper, tableAlias));
        }
        public <T>UserInfoJoin<T,R> RIGHT_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserInfoJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.RIGHT_JOIN,baseMapper, tableAlias));
        }
        public <T>UserInfoJoin<T,R> INNER_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserInfoJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.INNER_JOIN,baseMapper, tableAlias));
        }
        public UserInfoOrderBy<R> ORDER_BY() {
            return new UserInfoOrderBy<>(dataPackage);
        }
        public SelectExecution<R> EXEC() {
            return new SelectExecution<>(dataPackage);
        }
        public UserInfoWhere<SelectExecution<R>> WHERE() {
            return new UserInfoWhere<>(dataPackage, new SelectExecution<>(dataPackage));
        }
    }

    public static class UserInfoJoin<P,R> extends UserInfoSelect<R>{
        private final DataPackage.JoinInfo joinInfo;
        public UserInfoJoin(DataPackage dataPackage,DataPackage.JoinInfo joinInfo) {
            this.dataPackage = dataPackage;
            this.joinInfo = joinInfo;
        }
        public UserInfoJoin<P,R> on(String leftField, String rightField) {
            return ConditionSetting.joinOn(this,joinInfo,dataPackage,leftField, rightField);
        }
        public UserInfoJoin<P,R> on(BaseMapper<P> leftCondition, BaseMapper rightCondition) {
            return ConditionSetting.joinOn(this,joinInfo,leftCondition, rightCondition);
        }
        public UserInfoJoin<P,R> and(BaseMapper<P> baseMapper){
            return ConditionSetting.joinCondition(this,DataPackage.Way.AND,joinInfo,baseMapper);
        }
        public UserInfoJoin<P,R> or(BaseMapper<P> baseMapper){
             return ConditionSetting.joinCondition(this,DataPackage.Way.OR,joinInfo,baseMapper);
        }
    }

    public static class UserInfoOrderBy<R> extends UserInfoFields<OrderBy<UserInfoOrderBy<R>>> {
        public UserInfoOrderBy(DataPackage dataPackage) {
            this.dataPackage = dataPackage;
            this.t = new OrderBy<>(dataPackage, this);
        }
        public SelectExecution<R> EXEC() {
            return new SelectExecution<>(dataPackage);
        }
        public UserInfoWhere<SelectExecution<R>> WHERE() {
            return new UserInfoWhere<>(dataPackage, new SelectExecution<>(dataPackage));
        }
    }
    public static class UserInfoWhere<F> extends UserInfoFields<Where<UserInfoWhere<F>, UserInfo>> {
        private final F f;
        private UserInfoWhere(DataPackage dataPackage, F f) {
            this.dataPackage = dataPackage;
            this.t = new Where<>(dataPackage, this);
            this.f = f;
        }
        public UserInfoWhere<F> leftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.CUSTOM);
            return this;
        }
        public UserInfoWhere<F> orLeftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.OR);
            return this;
        }
        public UserInfoWhere<F> andLeftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.AND);
            return this;
        }
        public UserInfoWhere<F> rightBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.RightBracket,DataPackage.Way.CUSTOM);
            return this;
        }
        public UserInfoWhere<F> eqToObj(Object obj){
            ConditionSetting.setObj(dataPackage,obj);
            return this;
        }
        public UserInfoWhere<F> closeLogicDeleteProtect(){
            dataPackage.setLogicDelete(Boolean.FALSE);
            return this;
        }
        public F EXEC() {
            return f;
        }
    }
    public static class UserInfoFields<T> extends BaseMapper<UserInfo>{
        protected T t;
        /**
        *主键
        */
        public T id(){
            dataPackage.setField("id");
            return t;
        }
        /**
        *用户ID
        */
        public T userId(){
            dataPackage.setField("userId");
            return t;
        }
        /**
        *电话
        */
        public T phone(){
            dataPackage.setField("phone");
            return t;
        }
        /**
        *创建时间
        */
        public T createTime(){
            dataPackage.setField("createTime");
            return t;
        }
        /**
        *更新时间
        */
        public T updateTime(){
            dataPackage.setField("updateTime");
            return t;
        }
        /**
        *是否删除
        */
        public T deleted(){
            dataPackage.setField("deleted");
            return t;
        }
    }
}