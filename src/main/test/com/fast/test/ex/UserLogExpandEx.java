package com.fast.test.ex;

import com.fast.orm.data.*;
import com.fast.orm.exec.DeleteExecution;
import com.fast.orm.exec.InsertExecution;
import com.fast.orm.exec.SelectExecution;
import com.fast.orm.exec.UpdateExecution;
import com.fast.orm.many.JoinDirection;
import com.fast.orm.utils.FastValueUtil;
import com.fast.test.pojo.UserLogExpand;

/**
* 用户日志扩展
*/
public class UserLogExpandEx {
    public static UserLogExpandInsert INSERT() {
        return new UserLogExpandInsert();
    }
    public static UserLogExpandDelete DELETE() {
        return new UserLogExpandDelete();
    }
    public static UserLogExpandUpdate UPDATE() {
        return new UserLogExpandUpdate();
    }
    public static UserLogExpandSelect<UserLogExpand> SELECT() {
        return new UserLogExpandSelect<>();
    }
    public static <R>UserLogExpandSelect<R> SELECT(Class<R> returnClass) {
        return new UserLogExpandSelect<>(returnClass);
    }
    public static class UserLogExpandInsert{
        private final DataPackage dataPackage;
        public UserLogExpandInsert() {
            this.dataPackage = new DataPackage(UserLogExpand.class);
        }
        public InsertExecution<UserLogExpand> EXEC() {
            return new InsertExecution<>(dataPackage);
        }
    }
    public static class UserLogExpandDelete {
        private final DataPackage dataPackage;
        private UserLogExpandDelete() {
            this.dataPackage = new DataPackage(UserLogExpand.class);
        }
        public DeleteExecution EXEC() {
            return new DeleteExecution(dataPackage);
        }
        public UserLogExpandWhere<DeleteExecution> WHERE() {
            return new UserLogExpandWhere<>(dataPackage, new DeleteExecution(dataPackage));
        }
    }
    public static class UserLogExpandUpdate extends UserLogExpandFields<Update<UserLogExpandUpdate>> {
        private UserLogExpandUpdate() {
            this.dataPackage = new DataPackage(UserLogExpand.class);
            this.t = new Update<>(dataPackage, this);
        }
        public UserLogExpandUpdate set(UserLogExpand pojo){
            FastValueUtil.setUpdateBeanValue(dataPackage, pojo);
            return this;
        }
        public UpdateExecution<UserLogExpand> EXEC() {
            return new UpdateExecution<>(dataPackage);
        }
        public UserLogExpandWhere<UpdateExecution<UserLogExpand>> WHERE() {
            return new UserLogExpandWhere<>(dataPackage, new UpdateExecution<>(dataPackage));
        }
    }
    public static class UserLogExpandSelect<R> extends UserLogExpandFields<Select<UserLogExpandSelect<R>>> {
        private UserLogExpandSelect() {
            this.dataPackage = new DataPackage(UserLogExpand.class);
            this.t = new Select<>(dataPackage, this);
        }
        private UserLogExpandSelect(Class<R> returnClass) {
            this.dataPackage = new DataPackage(UserLogExpand.class,returnClass);
            this.t = new Select<>(dataPackage, this);
        }
        public <T>UserLogExpandJoin<T,R> LEFT_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserLogExpandJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.LEFT_JOIN,baseMapper, tableAlias));
        }
        public <T>UserLogExpandJoin<T,R> RIGHT_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserLogExpandJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.RIGHT_JOIN,baseMapper, tableAlias));
        }
        public <T>UserLogExpandJoin<T,R> INNER_JOIN(BaseMapper<T> baseMapper, String... tableAlias) {
            return new UserLogExpandJoin<>(dataPackage,ConditionSetting.joinInfo(dataPackage, JoinDirection.INNER_JOIN,baseMapper, tableAlias));
        }
        public UserLogExpandOrderBy<R> ORDER_BY() {
            return new UserLogExpandOrderBy<>(dataPackage);
        }
        public SelectExecution<R> EXEC() {
            return new SelectExecution<>(dataPackage);
        }
        public UserLogExpandWhere<SelectExecution<R>> WHERE() {
            return new UserLogExpandWhere<>(dataPackage, new SelectExecution<>(dataPackage));
        }
    }

    public static class UserLogExpandJoin<P,R> extends UserLogExpandSelect<R>{
        private final DataPackage.JoinInfo joinInfo;
        public UserLogExpandJoin(DataPackage dataPackage,DataPackage.JoinInfo joinInfo) {
            this.dataPackage = dataPackage;
            this.joinInfo = joinInfo;
        }
        public UserLogExpandJoin<P,R> on(String leftField, String rightField) {
            return ConditionSetting.joinOn(this,joinInfo,dataPackage,leftField, rightField);
        }
        public UserLogExpandJoin<P,R> on(BaseMapper<P> leftCondition, BaseMapper rightCondition) {
            return ConditionSetting.joinOn(this,joinInfo,leftCondition, rightCondition);
        }
        public UserLogExpandJoin<P,R> and(BaseMapper<P> baseMapper){
            return ConditionSetting.joinCondition(this,DataPackage.Way.AND,joinInfo,baseMapper);
        }
        public UserLogExpandJoin<P,R> or(BaseMapper<P> baseMapper){
             return ConditionSetting.joinCondition(this,DataPackage.Way.OR,joinInfo,baseMapper);
        }
    }

    public static class UserLogExpandOrderBy<R> extends UserLogExpandFields<OrderBy<UserLogExpandOrderBy<R>>> {
        public UserLogExpandOrderBy(DataPackage dataPackage) {
            this.dataPackage = dataPackage;
            this.t = new OrderBy<>(dataPackage, this);
        }
        public SelectExecution<R> EXEC() {
            return new SelectExecution<>(dataPackage);
        }
        public UserLogExpandWhere<SelectExecution<R>> WHERE() {
            return new UserLogExpandWhere<>(dataPackage, new SelectExecution<>(dataPackage));
        }
    }
    public static class UserLogExpandWhere<F> extends UserLogExpandFields<Where<UserLogExpandWhere<F>, UserLogExpand>> {
        private final F f;
        private UserLogExpandWhere(DataPackage dataPackage, F f) {
            this.dataPackage = dataPackage;
            this.t = new Where<>(dataPackage, this);
            this.f = f;
        }
        public UserLogExpandWhere<F> leftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.CUSTOM);
            return this;
        }
        public UserLogExpandWhere<F> orLeftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.OR);
            return this;
        }
        public UserLogExpandWhere<F> andLeftBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.LeftBracket,DataPackage.Way.AND);
            return this;
        }
        public UserLogExpandWhere<F> rightBracket() {
            ConditionSetting.setBracket(dataPackage,Expression.RightBracket,DataPackage.Way.CUSTOM);
            return this;
        }
        public UserLogExpandWhere<F> eqToObj(Object obj){
            ConditionSetting.setObj(dataPackage,obj);
            return this;
        }
        public UserLogExpandWhere<F> closeLogicDeleteProtect(){
            dataPackage.setLogicDelete(Boolean.FALSE);
            return this;
        }
        public F EXEC() {
            return f;
        }
    }
    public static class UserLogExpandFields<T> extends BaseMapper<UserLogExpand>{
        protected T t;
        /**
        *主键
        */
        public T id(){
            dataPackage.setField("id");
            return t;
        }
        /**
        *用户ID
        */
        public T userLogId(){
            dataPackage.setField("userLogId");
            return t;
        }
        /**
        *日志内容
        */
        public T expandInfo(){
            dataPackage.setField("expandInfo");
            return t;
        }
        /**
        *创建时间
        */
        public T createTime(){
            dataPackage.setField("createTime");
            return t;
        }
        /**
        *更新时间
        */
        public T updateTime(){
            dataPackage.setField("updateTime");
            return t;
        }
        /**
        *是否删除
        */
        public T deleted(){
            dataPackage.setField("deleted");
            return t;
        }
    }
}