package com.fast.test;
import com.fast.orm.generator.FastFileUtil;
import com.fast.orm.generator.TableFileCreateUtils;
import com.fast.orm.generator.bean.FileCreateConfig;

/**
 * 文件创建
 */
public class CreateFileTest {
    public static void main(String[] args) {
        FastFileUtil config = new FastFileUtil();
        //数据库连接
        config.setDBInfo("jdbc:mysql://192.168.1.101:3306/my_test?useUnicode=true&characterEncoding=utf-8&serverTimezone=Asia/Shanghai",
                "my_test","my_test",
                "com.mysql.cj.jdbc.Driver");
        //文件生成的包路径
        config.setBasePackage("com.fast.orm.test");
        //多模块项目时设置在哪个模块下生成文件
//        config.setChildModuleName("模块名称");
        //选择生成的文件
        config.setNeedModules(FileCreateConfig.CodeCreateModule.All);
////        //是否生成表前缀
//        config.setPrefix(false,false,null);
////        //是否使用lombok插件,默认false
        config.setUseLombok(false);
//        config.setExInlinePojo(false);
//        //是否在DTO上使用Swagger2注解,默认false
//        config.setUseDTOSwagger2(false);
//        //是否在POJO上使用Swagger2注解,默认false
        config.setUsePOJOSwagger2(false);
//        //DOT是否继承POJO
//        config.setDtoExtendsPOJO(true);
//        //需要生成的表名 (可选值,具体表名"tab1","tab2"或all)
//        config.setCreateTables("all");
        //生成代码
        config.create();
    }
}
