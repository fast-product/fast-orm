package com.fast.test;

import com.fast.orm.many.JoinQuery;
import com.fast.orm.mapper.NotQuery;
import com.fast.orm.mapper.TableAlias;
import com.fast.test.pojo.User;
import com.fast.test.pojo.UserLog;
import com.fast.test.pojo.UserType;

import java.io.Serializable;
import java.util.List;

/**
 * 用户多表查询DTO
 */
@TableAlias("user_test")
public class UserDTO extends User implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotQuery
    private Boolean deleted;

    @JoinQuery
    private List<UserLogDTO> userLogList;

    @JoinQuery(tableAlias = "log2")
    private UserLog userLog2;

    @JoinQuery(UserType.class)
    private UserTypeDTO userType;

    @JoinQuery(value = UserType.class,columnName = "type_name")
    private String type;

    public List<UserLogDTO> getUserLogList() {
        return userLogList;
    }

    public void setUserLogList(List<UserLogDTO> userLogList) {
        this.userLogList = userLogList;
    }

    public UserTypeDTO getUserType() {
        return userType;
    }

    public void setUserType(UserTypeDTO userType) {
        this.userType = userType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UserLog getUserLog2() {
        return userLog2;
    }

    public void setUserLog2(UserLog userLog2) {
        this.userLog2 = userLog2;
    }

}
