package com.fast.test.pojo;

import com.fast.orm.mapper.FastBean;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
* 用户日志扩展
*/
@Table(name = "user_log_expand")
@FastBean
public class UserLogExpand implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    *主键
    */
    @Id
    @Column(name = "id")
    private Long id;

    /**
    *用户ID
    */
    @Column(name = "user_log_id")
    private Long userLogId;

    /**
    *日志内容
    */
    @Column(name = "expand_info")
    private String expandInfo;

    /**
    *创建时间
    */
    @Column(name = "create_time")
    private Date createTime;

    /**
    *更新时间
    */
    @Column(name = "update_time")
    private Date updateTime;

    /**
    *是否删除
    */
    @Column(name = "deleted")
    private Boolean deleted;


    public Boolean getDeleted() {
        return this.deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUserLogId() {
        return this.userLogId;
    }
    public void setUserLogId(Long userLogId) {
        this.userLogId = userLogId;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getExpandInfo() {
        return this.expandInfo;
    }
    public void setExpandInfo(String expandInfo) {
        this.expandInfo = expandInfo;
    }

}
