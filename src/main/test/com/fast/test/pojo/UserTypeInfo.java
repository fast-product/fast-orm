package com.fast.test.pojo;

import com.fast.orm.mapper.FastBean;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
* 用户类型信息
*/
@Table(name = "user_type_info")
@FastBean
public class UserTypeInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    *主键
    */
    @Id
    @Column(name = "id")
    private Long id;

    /**
    *用户类型ID
    */
    @Column(name = "user_type_id")
    private Long userTypeId;

    /**
    *类型信息
    */
    @Column(name = "type_info")
    private String typeInfo;

    /**
    *创建时间
    */
    @Column(name = "create_time")
    private Date createTime;

    /**
    *更新时间
    */
    @Column(name = "update_time")
    private Date updateTime;

    /**
    *是否删除
    */
    @Column(name = "deleted")
    private Boolean deleted;


    public Long getUserTypeId() {
        return this.userTypeId;
    }
    public void setUserTypeId(Long userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getTypeInfo() {
        return this.typeInfo;
    }
    public void setTypeInfo(String typeInfo) {
        this.typeInfo = typeInfo;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }

}
