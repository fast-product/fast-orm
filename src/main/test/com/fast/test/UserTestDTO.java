package com.fast.test;

import java.io.Serializable;
import java.util.Date;

/**
* 用户
*/
public class UserTestDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    *主键
    */
    private Long id;

    /**
    *用户类型
    */
    private Long typeId;

    /**
    *用户名
    */
    private String userName;

    /**
    *年龄
    */
    private Integer age;

    /**
    *创建时间
    */
    private Date createTime;

    /**
    *更新时间
    */
    private Date updateTime;

    /**
    *是否删除
    */
    private Boolean deleted;


    public Boolean getDeleted() {
        return this.deleted;
    }
    public UserTestDTO setDeleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public Date getCreateTime() {
        return this.createTime;
    }
    public UserTestDTO setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public Long getTypeId() {
        return this.typeId;
    }
    public UserTestDTO setTypeId(Long typeId) {
        this.typeId = typeId;
        return this;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }
    public UserTestDTO setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public Long getId() {
        return this.id;
    }
    public UserTestDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUserName() {
        return this.userName;
    }
    public UserTestDTO setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public Integer getAge() {
        return this.age;
    }
    public UserTestDTO setAge(Integer age) {
        this.age = age;
        return this;
    }

}
