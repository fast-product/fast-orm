package com.fast.test;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fast.orm.cache.RedisCacheImpl;
import com.fast.orm.config.FastAttributes;
import com.fast.orm.exec.SelectExecution;
import com.fast.orm.many.AutoQuery;
import com.fast.orm.many.JoinQuery;
import com.fast.orm.utils.page.PageInfo;
import com.fast.test.ex.UserEx;
import com.fast.test.ex.UserLogEx;
import com.fast.test.ex.UserLogExpandEx;
import com.fast.test.ex.UserTypeEx;
import com.fast.test.pojo.*;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

public class AutoQueryTest extends User {

    static {
        //加载配置文件
        FastSetConfigTest.fastConfig();
    }

    public static void main(String[] args) {
        UserLogEx.UserLogWhere<SelectExecution<UserLog>> logWhere = UserLogEx.SELECT().WHERE();
        UserLogExpandEx.UserLogExpandWhere<SelectExecution<UserLogExpand>> expandWhere = UserLogExpandEx.SELECT().WHERE();
        PageInfo<AutoQueryTest> page = UserEx.SELECT(AutoQueryTest.class)
//                .LEFT_JOIN(logWhere)
//                .LEFT_JOIN(expandWhere).on(expandWhere.userLogId(),logWhere.id())
                .EXEC().findPage(1, 10);
        System.out.println(JSONObject.toJSONString(page.getList()));
//        System.out.println(JSONObject.toJSONString(one));
//        for (int i = 0; i < 10; i++) {
//            UserEx.SELECT(AutoQueryTest.class).EXEC().findAll();
//            if (i==5){
//                UserEx.UPDATE().userName().set("张三").WHERE().id().eqTo(1).EXEC().update();
//            }
//        }
    }

//    @AutoQuery(value = UserType.class, queryField = "typeName")
    private String type;

//    @AutoQuery(thisField = "id", targetField = "userTypeId")
    private UserType userType;

//    @AutoQuery(UserInfo.class)
    private String phone;

//    @AutoQuery(thisField = "userId", targetField = "id")
    private UserInfo userInfo;

//    @AutoQuery(thisField = "id", targetField = "pid")
    private User user;


    @AutoQuery
    @JoinQuery
    private List<UserLogDTO> userLogList;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<UserLogDTO> getUserLogList() {
        return userLogList;
    }

    public void setUserLogList(List<UserLogDTO> userLogList) {
        this.userLogList = userLogList;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
