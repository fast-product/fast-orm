package com.fast.test;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import com.fast.orm.exec.FastCustomQuery;
import com.fast.test.pojo.User;
import com.fast.orm.utils.page.PageInfo;
import com.fast.test.ex.UserEx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserTest {
    static {
        //加载配置文件
        FastSetConfigTest.fastConfig();
    }

    public static void main(String[] args) {
//        insert();
//        insertList();
//        findCount();
//        findByPrimaryKey();
//        findOne();
//        findAll();
//        findPage();
//        update();
//        updateOverall();
//        updateOverall();
//        updateByPrimaryKey();
//        updateOverallByPrimaryKey();
//        delete();
//        deleteByPrimaryKey();
//        leftBracket();
//        customSql();
        UserEx.SELECT().age().show().age().groupBy().EXEC().findAll();

    }

    public static void cache() {
        String uuid = IdUtil.fastSimpleUUID();
        for (int i = 0; i < 10; i++) {
            System.out.println(i + 1);
            findPage();
        }
        update();
        for (int i = 0; i < 10; i++) {
            System.out.println(i + 1);
            findPage();
        }
        delete();
        for (int i = 0; i < 10; i++) {
            System.out.println(i + 1);
            findPage();
        }
    }

    public static void customSql() {
        String sql = "SELECT * FROM user WHERE `user_name` LIKE ${userName}";
        HashMap<String, Object> params = new HashMap<>();
        params.put("userName", "%张三%");
        PageInfo<User> page = FastCustomQuery.create(User.class, sql, params).findPage(1, 10);
    }

    public static void leftBracket() {
        UserEx.SELECT().WHERE()
                .id().eqTo(1)
                .andLeftBracket()
                .leftBracket()
                .userName().eqTo("Fast1").age().between(1, 10)
                .rightBracket()
                .orLeftBracket()
                .userName().eqTo("Fast2").age().or().greater(1)
                .rightBracket()
                .andLeftBracket()
                .userName().eqTo("Fast3").age().greater(3)
                .rightBracket()
                .rightBracket()
                .age().eqTo(1)
                .EXEC().findAll();
    }

    public static void delete() {
        UserEx.DELETE().WHERE().closeLogicDeleteProtect().userName().eqTo("Fast110").EXEC().delete();
    }

    public static void deleteByPrimaryKey() {
        UserEx.DELETE().EXEC().deleteByPrimaryKey(111);
    }

    public static void updateOverallByPrimaryKey() {
        User user = new User();
        user.setId(103L);
        user.setUserName("Fast103-updateOverallByPrimaryKey");
        UserEx.UPDATE().set(user).EXEC().updateOverallByPrimaryKey(103);
    }

    public static void updateByPrimaryKey() {
        User user = new User();
        user.setUserName("Fast102-updateByPrimaryKey");
        UserEx.UPDATE().set(user).EXEC().updateByPrimaryKey(102);
    }

    public static void updateOverall() {
        UserEx.UPDATE().userName().set("Fast101-updateOverall").id().set(101).WHERE().id().eqTo(101).userName().likeRight("Fast").EXEC().updateOverall();
    }

    public static void update() {
        UserEx.UPDATE()
                .userName().set("张三")
                .WHERE().id().eqTo(1)
                .EXEC().update();
    }

    public static void insert() {
        for (int i = 1; i <= 20; i++) {
            User user = new User();
            user.setId(Long.parseLong(i + ""));
            user.setUserName("Fast" + i);
            user.setAge(i);
            User insert = UserEx.INSERT().EXEC().insert(user);
        }
    }

    public static void insertList() {
        List<User> userList = new ArrayList<>();
        for (int i = 100; i <= 120; i++) {
            User user = new User();
            user.setId(Long.parseLong(i + ""));
            user.setUserName("Fast" + i);
            user.setAge(i);
            userList.add(user);
        }
        List<User> ls = UserEx.INSERT().EXEC().insert(userList);
    }

    public static void findCount() {
        UserEx.SELECT().age().hide()
                .WHERE()
                .userName().likeRight("Fast")
                .userName().in("Fast1", "Fast2")
                .age().between(1, 10)
                .EXEC().findCount();
    }

    public static void findByPrimaryKey() {
        UserEx.SELECT().EXEC().findByPrimaryKey(1);
    }

    public static void findOne() {
        UserEx.SELECT().age().hide()
                .ORDER_BY()
                .userName().desc().age().asc()
                .WHERE()
                .userName().likeRight("Fast")
                .userName().in("Fast1", "Fast2")
                .age().between(1, 10)
                .EXEC().findOne();
    }

    public static void findAll() {
        UserEx.SELECT().age().hide()
                .ORDER_BY()
                .userName().desc().age().asc()
                .WHERE()
                .userName().likeRight("Fast")
                .userName().in("Fast1", "Fast2")
                .age().between(1, 10)
                .EXEC().findAll();
    }

    public static void findPage() {
        PageInfo<User> pageInfo = UserEx.SELECT().id().show().userName().show()
                .ORDER_BY()
                .age().asc()
                .WHERE()
                .userName().likeRight("Fast")
                .userName().in("Fast1", "Fast2")
                .age().between(1, 10)
                .EXEC().findPage(1, 10);
    }
}
