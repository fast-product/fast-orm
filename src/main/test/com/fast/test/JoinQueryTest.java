package com.fast.test;

import cn.hutool.core.collection.CollUtil;
import com.fast.orm.exec.FastCustomQuery;
import com.fast.orm.exec.SelectExecution;
import com.fast.orm.utils.page.PageInfo;
import com.fast.test.ex.*;
import com.fast.test.pojo.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JoinQueryTest {

    static {
        //加载配置文件
        FastSetConfigTest.fastConfig();
    }


    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            findInfo();
        }
//        fastCustomSqlDao();
//        findAll();
//        joinsingleTableQuery();
    }

    public static void findInfo() {
        UserLogEx.UserLogWhere<SelectExecution<UserLog>> logWhere = UserLogEx.SELECT().WHERE().logInfo().likeRight("Log");
        UserLogExpandEx.UserLogExpandWhere<SelectExecution<UserLogExpand>> expandWhere = UserLogExpandEx.SELECT().WHERE();
        UserTypeEx.UserTypeWhere<SelectExecution<UserType>> typeWhere = UserTypeEx.SELECT().ORDER_BY().createTime().desc().WHERE().closeLogicDeleteProtect();
        UserTypeInfoEx.UserTypeInfoWhere<SelectExecution<UserTypeInfo>> typeInfoWhere = UserTypeInfoEx.SELECT().WHERE();
        UserEx.SELECT(UserDTO.class)
                .LEFT_JOIN(typeWhere)
                .LEFT_JOIN(typeInfoWhere).on(typeInfoWhere.userTypeId(),typeWhere.id())
                .LEFT_JOIN(logWhere)
                .LEFT_JOIN(expandWhere).on(expandWhere.userLogId(),logWhere.id())
                .ORDER_BY().createTime().desc().updateTime().asc()
                .WHERE().age().lessOrEqual(100).userName().likeRight("User")
                .userTypeId().eqTo(typeWhere.id())
                .closeLogicDeleteProtect()
                .EXEC().findPage(1, 10);
    }

    public static void findAll() {
        //设置用户查询条件
        UserEx.UserSelect<UserDTO> userQuery = UserEx.SELECT(UserDTO.class);
        //设置用户类型查询条件
        UserTypeEx.UserTypeWhere<SelectExecution<UserType>> typeQuery = UserTypeEx.SELECT().WHERE().typeName().likeRight("Type");
        //设置用户日志1查询条件
        UserLogEx.UserLogWhere<SelectExecution<UserLog>> logQuery = UserLogEx.SELECT().WHERE();
        UserTypeInfoEx.UserTypeInfoWhere<SelectExecution<UserTypeInfo>> typeInfoQuery = UserTypeInfoEx.SELECT().WHERE();
        //设置用户日志2查询条件
        UserLogEx.UserLogWhere<SelectExecution<UserLog>> logQuery2 = UserLogEx.SELECT().WHERE();
        userQuery.LEFT_JOIN(typeQuery).on(typeQuery.id(), userQuery.userTypeId())
                .LEFT_JOIN(logQuery)
                .LEFT_JOIN(logQuery2,"log2")
                .LEFT_JOIN(typeInfoQuery).on(typeInfoQuery.userTypeId(),typeQuery.id())
                .ORDER_BY().age().desc().WHERE().userName().like("User").age().greaterOrEqual(1).id().between(1, 10)
                .EXEC().findAll();
    }

    public static void joinsingleTableQuery() {
        PageInfo<UserTestDTO> page = UserEx.SELECT(UserTestDTO.class).WHERE().EXEC().findPage(1, 10);
    }

    public static void fastCustomSqlDao() {
        String sql = "SELECT u.id,u.user_name,u.user_type_id,t.id,t.type_name FROM user u LEFT JOIN user_type t on u.user_type_id = t.id where u.id=${id}";
        Map<String, Object> data = new HashMap<>();
        data.put("id", 1);
        PageInfo<CustomSqlUserDTO> all = FastCustomQuery.create(CustomSqlUserDTO.class, sql, data).findPage(1, 10);
    }

    public static void addData() {
        User user1 = new User();
        user1.setUserTypeId(1L);
        user1.setDeleted(false);
        user1.setCreateTime(new Date());
        user1.setUpdateTime(new Date());
        user1.setId(1L);
        user1.setUserName("用户1");
        user1.setAge(1);
        User user2 = new User();
        user2.setUserTypeId(1L);
        user2.setDeleted(false);
        user2.setCreateTime(new Date());
        user2.setUpdateTime(new Date());
        user2.setId(2L);
        user2.setUserName("用户2");
        user2.setAge(2);
        User user3 = new User();
        user3.setUserTypeId(2L);
        user3.setDeleted(false);
        user3.setCreateTime(new Date());
        user3.setUpdateTime(new Date());
        user3.setId(3L);
        user3.setUserName("用户3");
        user3.setAge(3);
        UserEx.INSERT().EXEC().insert(CollUtil.newArrayList(user1, user2, user3));

        UserType userType1 = new UserType();
        userType1.setDeleted(false);
        userType1.setCreateTime(new Date());
        userType1.setTypeName("类型1");
        userType1.setUpdateTime(new Date());
        userType1.setId(1L);
        UserType userType2 = new UserType();
        userType2.setDeleted(false);
        userType2.setCreateTime(new Date());
        userType2.setTypeName("类型2");
        userType2.setUpdateTime(new Date());
        userType2.setId(2L);
        UserTypeEx.INSERT().EXEC().insert(CollUtil.newArrayList(userType1, userType2));

        UserLog userLog1 = new UserLog();
        userLog1.setDeleted(false);
        userLog1.setCreateTime(new Date());
        userLog1.setUpdateTime(new Date());
        userLog1.setId(1L);
        userLog1.setLogInfo("日志1");
        userLog1.setUserId(1L);
        UserLog userLog2 = new UserLog();
        userLog2.setDeleted(false);
        userLog2.setCreateTime(new Date());
        userLog2.setUpdateTime(new Date());
        userLog2.setId(2L);
        userLog2.setLogInfo("日志2");
        userLog2.setUserId(1L);
        UserLogEx.INSERT().EXEC().insert(CollUtil.newArrayList(userLog1, userLog2));
    }
}
