package com.fast.test;

import com.fast.orm.aspect.ExpanderOccasion;
import com.fast.orm.aspect.FastExpander;
import com.fast.orm.data.DataPackage;

import java.util.ArrayList;
import java.util.List;

public class DemoExpander implements FastExpander {
    /**
     * @param dataPackage 封装了DAO所有的执行参数
     * @return 是否执行
     */
    @Override
    public void before(DataPackage dataPackage) {
        System.out.println("DAO执行前");
    }

    /**
     * @param dataPackage 封装了DAO所有的执行参数
     */
    @Override
    public void after(DataPackage dataPackage) {
        System.out.println("DAO执行后");
    }

    @Override
    public List<ExpanderOccasion> occasion() {
        //配置DAO切面执行时机
        List<ExpanderOccasion> list = new ArrayList<>();
        list.add(ExpanderOccasion.SELECT);
        list.add(ExpanderOccasion.UPDATE);
        return list;
    }

}