package com.fast.test;

import com.fast.orm.many.JoinQuery;
import com.fast.test.pojo.User;
import com.fast.test.pojo.UserType;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 测试表
 */
@Table(name = "u")
public class CustomSqlUserDTO extends User implements Serializable {

    private static final long serialVersionUID = 1L;

    @JoinQuery(tableAlias = "t", columnName = "id")
    private UserType userType;

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }
}
