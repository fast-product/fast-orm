package com.fast.test;

import com.fast.orm.many.AutoQuery;
import com.fast.orm.many.JoinQuery;
import com.fast.test.pojo.User;
import com.fast.test.pojo.UserLog;
import com.fast.test.pojo.UserLogExpand;

import javax.persistence.Table;

@Table(name = "user_log")
public class UserLogDTO extends UserLog {

    @AutoQuery
    @JoinQuery
    private UserLogExpand expand;

    @AutoQuery(User.class)
    private String userName;

    public UserLogExpand getExpand() {
        return expand;
    }

    public void setExpand(UserLogExpand expand) {
        this.expand = expand;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
