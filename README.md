# Java 极·简ORM框架
>Gitee: [https://gitee.com/fast-product/fast-orm](https://gitee.com/fast-product/fast-orm)
>
>作者: 张亚伟
>有任何框架的使用问题和BUG可以直接在Issues上提出 基本都能快速解决
>邮箱: 398850094@qq.com

----
- 极简化
- 链式语法操作
- 多表查询
- 自动关联查询
- 分布式缓存和内存缓存
- 原生SQL查询
- 逻辑删除
- 至少提升30%综合开发效率

 **示例** 
```java
User user = UserEx.INSERT().EXEC().insert(user); //增
Integer delCount = UserEx.DELETE().EXEC().deleteByPrimaryKey(id); //删
Integer updateCount = UserEx.UPDATE().set(user).EXEC().updateByPrimaryKey(); //改
PageInfo<User> page = UserEx.SELECT().EXEC().findPage(1,10); //查
```

# 快速入门

## Maven安装
```xml
<dependency>
    <groupId>com.fast-orm</groupId>
    <artifactId>fast-orm</artifactId>
    <version>1.0.0</version>
</dependency>
```

## 文件生成
```java
    public static void main(String[] args) {
        FastFileUtil config = new FastFileUtil();
        //数据库连接
        config.setDBInfo(url,用户名,密码,"com.mysql.cj.jdbc.Driver");
        //文件生成的包路径
        config.setBasePackage("com.fast.orm.test");
        //生成代码
        config.create();
    }
```
### 其他配置
```java
        //多模块项目时设置在哪个模块下生成文件
        config.setChildModuleName("模块名称");
        //是否生成表前缀
        config.setPrefix(false,false,null);
        //是否使用lombok插件,默认false
        config.setUseLombok(false);
        //是否在DTO上使用Swagger2注解,默认false
        config.setUseDTOSwagger2(false);
        //是否在POJO上使用Swagger2注解,默认false
        config.setUsePOJOSwagger2(false);
        //DOT是否继承POJO
        config.setDtoExtendsPOJO(true);
        //需要生成的表名 (可选值,具体表名"tab1","tab2"或all)
        config.setCreateTables("all");
```
## 框架配置

### Spring环境
Spring环境下无需任何配置即可使用,框架可自动识别Spring配置的数据源信息

### 非Spring环境
```java
public static void fastConfig() {
    /**
     * 数据源配置
     */
    FastConfig.dataSource(数据源);
}

### 其他配置
#### 日志输出
```java
/**
 * 设置SQL日志打印,默认关闭
 * 参数1: 日志打印级别 DEBUG,INFO,OFF
 * 参数2: 是否打印详细SQL日志
 * 参数3: 是否打印SQL执行结果
 */
FastConfig.openSqlPrint(SqlLogLevel.INFO,true, true);
```
#### 逻辑删除
```java
/**
 * 开启逻辑删除功能,开启后会对逻辑删除标记的数据在 更新|删除|查询 时进行保护,可通过模板进行单次操作逻辑删除保护的关闭
 * 参数1:  逻辑删除字段名
 * 参数2:  逻辑删除标记默认值
 */
FastConfig.openLogicDelete("deleted", Boolean.TRUE);
```
#### 自动设置时间
```java
/**
 * 开启自动对数据 新增操作 进行创建时间设置
 * 参数1: 需要设置创建时间的字段名
 */
FastConfig.openAutoSetCreateTime("create_time");
/**
 * 开启自动对数据 更新操作/逻辑删除操作 进行更新时间设置
 * 参数1: 需要设置更新时间的字段名
 */
FastConfig.openAutoSetUpdateTime("update_time");
```

## 示例

### 新增
```
ID会Set到新增的对象中
```java
User user = new User();
user.setUserName("张三");
user.setAge(20);
user.setDeleted(false);
UserEx.INSERT().EXEC().insert(user)
```
```SQL输出```
```sql
INSERT INTO user SET user_name = '张三' , age = 20 , deleted = false
```
```执行结果```
```json
[{"age":20,"deleted":false,"id":189,"userName":"张三"}]
```

#### 批量新增
```java
User user1 = new User();
user1.setUserName("张三");
user1.setAge(20);
user1.setDeleted(false);

User user2 = new User();
user2.setUserName("李四");
user2.setAge(30);
user2.setDeleted(false);

List<User> userList = new ArrayList<>();
userList.add(user1);
userList.add(user2);
List<User> ls = UserEx.INSERT().EXEC().insert(userList);
```
```SQL输出```
```sql
INSERT INTO user
(`id`,`type_id`,`user_name`,`age`,`create_time`,`update_time`,`deleted`) VALUES
(null , null , '张三' , 20 , null , null , false ),
(null , null , '李四' , 30 , null , null , false )
```

### 删除
```
注意
如果设置逻辑删除则调用Updata
如果没设置逻辑删除则会调用Delete
返回值为删除条目数
```
#### 通过主键删除
```java
UserEx.DELETE().EXEC().deleteByPrimaryKey(1);
```
```SQL输出-物理删除```
```sql
DELETE FROM user WHERE `id` = 1
```
```SQL输出-逻辑删除```
```sql
UPDATE user SET `deleted` = true WHERE `deleted` = false AND `id` = 1
```

#### 通过条件删除
```java
Integer delete = UserEx.DELETE().WHERE().userName().eqTo("张三").EXEC().delete()
```
```SQL输出-物理删除```
```sql
DELETE FROM user WHERE `user_name` = '张三'
```
```SQL输出-逻辑删除```
```sql
UPDATE user SET  `deleted` = true  WHERE `deleted` = false AND `user_name` = '张三'
```
### 更新
```
返回值为更新条目数
```
```java
User user = new User();
user.setId(1);
user.setUserName("更新姓名");
UserEx.UPDATE().set(user).EXEC().updateByPrimaryKey()

//或
UserEx.UPDATE().userName().set("更新姓名").WHERE().id().eqTo(1).EXEC().update()
```
```SQL输出```
```sql
UPDATE user SET `user_name` = '更新姓名' WHERE `id` = 1
```


### 查询
#### 通过ID查询
```java
User user = UserEx.SELECT().EXEC().findByPrimaryKey(1);
```
```SQL输出```
```sql
SELECT `id`,`type_id`,`user_name`,`age`,`create_time`,`update_time`,`deleted`
FROM user
WHERE `id` = 1
LIMIT 1
```

#### 通过条件查询
```java
List<User> userList = UserEx.SELECT().WHERE().userName().like("张").EXEC().findAll();
```
```SQL输出```
```sql
SELECT `id`,`type_id`,`user_name`,`age`,`create_time`,`update_time`,`deleted`
FROM user
WHERE `user_name` LIKE '%张%'
```

#### 分页查询
```java
PageInfo<User> pageInfo = UserEx.SELECT().WHERE().userName().like("张").EXEC().findPage(1,10);
```
```SQL输出```
```sql
SELECT `id`,`type_id`,`user_name`,`age`,`create_time`,`update_time`,`deleted`
FROM user
WHERE `user_name` LIKE '%张%'
LIMIT 0 , 10
```

#### 自定义查询对象
```
如果自定义查询对象中使用了关联查询等注解 会根自动据注解功能进行操作
```
```java
List<UserDTO> userDTOList = UserEx.SELECT(UserDTO.class).WHERE().userName().like("张").EXEC().findAll();
```


### 原生SQL
```java
String sql = "SELECT * FROM user WHERE `user_name` LIKE ${userName}";
HashMap<String, Object> params = new HashMap<>();
params.put("userName", "%张三%");
PageInfo<User> page = FastCustomQuery.create(User.class, sql, params).findPage(1, 10);
```

```SQL输出```
```sql
SELECT * FROM user WHERE `user_name` LIKE '%张三%' LIMIT 0, 10
```

# 执行器EXEC
```java
映射器.执行方式().EXEC().方法();

//示例
List<User> userList = UserEx.SELECT().EXEC().findAll();
```

# 条件设置
```java
映射器.执行方式().WHERE().字段名().条件(条件参数);

//示例
List<User> userList = UserEx.SELECT().WHERE().userName().eqTo("张三").EXEC().findAll();
```

### 排序
```java
映射器.SELECT().ORDER_BY().字段名().排序方式()

//示例
List<User> userList = UserEx.SELECT().ORDER_BY().userName().asc().EXEC().findAll();
```

### 查询字段设置(支持隐藏,指定字段显示,去重,聚合函数等)
```java
映射器.SELECT().字段名().查询方式()

//示例
List<User> userList = UserEx.SELECT().userName().hide().EXEC().findAll();
```


### OR条件
```java
映射器.执行方式().WHERE().字段名().or();

//示例
List<User> userList = UserEx.SELECT().WHERE().userName().eqTo("张三").age().or().eqTo(10).EXEC().findAll();
```


### 添加括号
```java
映射器.执行方式().WHERE().括号();

//示例
List<User> userList = UserEx.SELECT().WHERE().userName().eqTo("张三")
    .andLeftBracket().userName().eqTo("李四").age().or().eqTo(10).rightBracket()
    .EXEC().findAll();
```
```sql
SELECT `id`,`user_type_id`,`user_name`,`age`,`pid`,`create_time`,`update_time`,`deleted`
FROM user
WHERE `deleted` = false
AND `user_name` = '张三'
AND (`user_name` = '李四' OR `age` = 10)
```
# 原生SQL

多表等复杂SQL操作,可以使用原生SQL执行器实现,框架会自动进行对象和表进行映射<br>
如果有参数需要使用 ${参数名} 声明,传递参数MAP集合中put(参数名,参数值)<br>
FastCustomQuery.create(Class, SQL语句, 参数)

```java
//例:
String sql = "SELECT * FROM user WHERE `user_name` LIKE ${userName}";
HashMap<String, Object> params = new HashMap<>();
params.put("userName","%张三%");
PageInfo<User> page = FastCustomQuery.create(User.class, sql, params).findPage(1, 10);
```


# 多表查询
```
如果多表间的关联列命名符合框架规范 关联表名_关联表主键名 这样的形式 框架可以自动进行关联条件的设置
例如 
A表(user) 列 id   
B表(type) 列 user_id
或
A表(user) 列 type_id   
B表(type) 列 id
```

```多表查询字段注解说明```
```java
//多表查询时自动对字段赋值
@JoinQuery
//自动查询的字段 设置后会对此字段进行字段查询 自动赋值
@AutoQuery
//不进行SQL操作的字段
@NotQuery
```
```java
//符合本框架命名规范可以无需设置关联条件 框架自动进行关联设置
 UserEx.SELECT()
.LEFT_JOIN(UserLogEx.SELECT())
.EXEC().findAll();


//手动设置关联条件
UserLogEx.UserLogWhere<SelectExecution<UserLog>> userLogQuery = UserLogEx.SELECT().WHERE();
UserEx.UserSelect<UserDTO> userQuery = UserEx.SELECT(UserDTO.class);
userQuery.LEFT_JOIN(userLogQuery).on(userLogQuery.userId(), userQuery.id())
.EXEC().findAll();
```
```示例```
```java
UserLogEx.UserLogWhere<SelectExecution<UserLog>> logWhere = UserLogEx.SELECT().WHERE().logInfo().likeRight("Log");
UserLogExpandEx.UserLogExpandWhere<SelectExecution<UserLogExpand>> expandWhere = UserLogExpandEx.SELECT().WHERE();
UserTypeEx.UserTypeWhere<SelectExecution<UserType>> typeWhere = UserTypeEx.SELECT().ORDER_BY().createTime().desc().WHERE().closeLogicDeleteProtect();
UserTypeInfoEx.UserTypeInfoWhere<SelectExecution<UserTypeInfo>> typeInfoWhere = UserTypeInfoEx.SELECT().WHERE();
UserEx.SELECT(UserDTO.class)
    .LEFT_JOIN(typeWhere)
    .LEFT_JOIN(typeInfoWhere).on(typeInfoWhere.userTypeId(),typeWhere.id())
    .LEFT_JOIN(logWhere)
    .LEFT_JOIN(expandWhere).on(expandWhere.userLogId(),logWhere.id())
    .ORDER_BY().createTime().desc().updateTime().asc()
    .WHERE().age().lessOrEqual(100).userName().likeRight("User")
    .closeLogicDeleteProtect()
    .EXEC().findPage(1, 10);
```
```SQL输出```
```sql
SELECT user.`id`,user.`user_type_id`,user.`user_name`,user.`age`,user.`pid`,user.`create_time`,user.`update_time`,user.`deleted`
     ,user_type.`id`,user_type.`type_name`,user_type.`create_time`,user_type.`update_time`,user_type.`deleted`,user_type.`type_name` AS type
     ,user_type_info.`id`,user_type_info.`user_type_id`,user_type_info.`type_info`,user_type_info.`create_time`,user_type_info.`update_time`,user_type_info.`deleted`
     ,user_log.`id`,user_log.`user_id`,user_log.`log_info`,user_log.`create_time`,user_log.`update_time`,user_log.`deleted`
     ,user_log_expand.`id`,user_log_expand.`user_log_id`,user_log_expand.`expand_info`,user_log_expand.`create_time`,user_log_expand.`update_time`,user_log_expand.`deleted`
FROM user AS user
         LEFT JOIN user_type AS user_type ON user_type.`id` = user.`user_type_id`
         LEFT JOIN user_type_info AS user_type_info ON user_type_info.`user_type_id` = user_type.`id` AND user_type_info.`deleted` = false
         LEFT JOIN user_log AS user_log ON user_log.`user_id` = user.`id` AND user_log.`deleted` = false
         LEFT JOIN user_log_expand AS user_log_expand ON user_log_expand.`user_log_id` = user_log.`id` AND user_log_expand.`deleted` = false
WHERE user.`age` <= 100
  AND user.`user_name` LIKE 'User%'
  AND user_log.`log_info` LIKE 'Log%'
ORDER BY user.`create_time` DESC,user.`update_time` ASC,user_type.`create_time` DESC
LIMIT 0, 10
```



# 缓存使用
开启缓存功能后,可以Bean添加注解的方式启用缓存

```java
/**
 * Redis缓存
 * 当进行使用此框架模板进行操作新增,更新,删除操作时,会自动刷新Redis缓存中的数据
 * 默认参数为框架设置的缓存时间和类型
 * 缓存可选参数
 * FastRedisCache(Long 秒) 如@FastRedisCache(60L) 缓存60秒
 * FastRedisCache(cacheTime = 时间,cacheTimeType = TimeUnit) 如@FastRedisCache(cacheTime =1,cacheTimeType = TimeUnit.HOURS) 缓存1小时
 */
@FastRedisCache

/**
 1. 内存缓存
 2. 当开启缓存并操作对象配置此注解时,会将查询到的数据缓存到本地中
 3. 当进行使用此框架模板进行操作新增,更新,删除操作时,会自动刷新内存中缓存的数据
 4. 默认参数为框架设置的缓存时间和类型
 5. 缓存可选参数
 6. FastStaticCache(Long 秒) 如@FastStaticCache(60L) 缓存60秒
 7. FastStaticCache(cacheTime = 时间,cacheTimeType = TimeUnit) 如@FastStaticCache(cacheTime =1,cacheTimeType = TimeUnit.HOURS) 缓存1小时
 */
@FastStaticCache
```

# 数据源切换

可以在任意一次执行时进行数据源更换,更换数据源只对当前线程影响

```java
//例
FastConfig.dataSource(数据源);//更换全局数据源
FastConfig.dataSourceThreadLocal(数据源);//更换本线程数据源

```
----

# 切面
### 配置切面实现,可以添加多个切面
```java
FastConfig.addExpander(DemoExpander.class);
```
使用切面可以进行很多自定义操作,比如读写分离,CRUD时候添加参数,权限验证等
### 实现FastExpander接口
```java
public class DemoExpander implements FastExpander {

    /**
     * @param dataPackage 封装了DAO所有的执行参数
     * @return 是否执行
     */
    @Override
    public void before(DataPackage dataPackage) {
        System.out.println("DAO执行前");
    }

    /**
     * @param dataPackage 封装了DAO所有的执行参数
     */
    @Override
    public void after(DataPackage dataPackage) {
        System.out.println("DAO执行后");
    }

    @Override
    public List<ExpanderOccasion> occasion() {
        //配置DAO切面执行时机
        List<ExpanderOccasion> list = new ArrayList<>();
        list.add(ExpanderOccasion.SELECT);
        list.add(ExpanderOccasion.UPDATE);
        return list;
    }

}
```


# 手动事务管理
```java
FastTransaction.open(); //开启事务
FastTransaction.commit(); //提交
FastTransaction.rollback(); //回滚
```
**感谢使用,希望您能提出宝贵的建议,我会不断改进更新**